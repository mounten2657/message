<?php

return [

    'enabled' => env('DING_ENABLED',true),

    'token' => env('DING_TOKEN','')
];