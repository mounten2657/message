<?php

use \Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// 接口根目录，默认返回服务器时间
Route::any('/', function () {
    return response()->json(['server_time' => time()]);
});

// 需要签名验证的业务
Route::group(['middleware' => 'sign'], function() {

    // Api 业务路由组
    Route::group(['namespace' => 'Api'], function () {
        // 短信 - 发送短信验证码
        Route::post('/sms/code', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Api\SmsController::Ins()->send($request);
        });
        // 短信 - 验证短信验证码
        Route::post('/sms/verify', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Api\SmsController::Ins()->verifyCode($request);
        });
        // 短信 - 统一发送接口
        Route::post('/sms/union', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Api\SmsController::Ins()->union($request);
        });

        // 钉钉 - 钉钉工作通知
        Route::post('/dingtalk/notice', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Api\DingTalkController::Ins()->send($request);
        });

        // 获取推送状态
        Route::post('/task/status', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Api\TaskController::Ins()->getStatus($request);
        });

        // 整合 - 统一发送接口
        Route::post('/union/send', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Api\UnionController::Ins()->send($request);
        });
    });

    // Admin 业务路由组
    Route::group(['namespace' => 'Admin'], function () {
        // 推送统计 - 统计数据
        Route::post('/statistic/data', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\StatController::Ins()->getData($request);
        });

        // 推送列表 - 列表数据
        Route::post('/search/list', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SearchController::Ins()->getlist($request);
        });
        // 推送列表 - 类型列表
        Route::post('/search/typeList', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SearchController::Ins()->getTypeList($request);
        });

        // 推送配置 - 钉钉配置
        Route::post('/setting/dingtalk', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->getList($request);
        });
        // 推送配置 - 短信配置
        Route::post('/setting/sms', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->getList($request);
        });
        // 推送配置 - Key值管理
        Route::post('/setting/key', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->getList($request);
        });
        // 推送配置 - 信息保存
        Route::post('/setting/save', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->save($request);
        });
        // 推送配置 - 关闭场景
        Route::post('/setting/offScene', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->save($request);
        });
        // 推送配置 - 刷新Key值
        Route::post('/setting/refreshKey', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->save($request);
        });
        // 推送配置 - 队列设置
        Route::post('/setting/queue', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->queue($request);
        });
        // 推送配置 - 队列重启
        Route::post('/setting/queue/reload', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->queueReload($request);
        });
        // 推送配置 - 系统信息
        Route::post('/setting/info', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->info($request);
        });
        // 推送配置 - 钉钉类型
        Route::post('/setting/dingtalk/type', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->getTypeList($request);
        });
        // 推送配置 - 短信类型
        Route::post('/setting/sms/type', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Admin\SettingController::Ins()->getTypeList($request);
        });
    });

});

// 不需要签名验证的业务
Route::any('/test', function (\Illuminate\Http\Request $request) {
    return \App\Http\Controllers\Api\TestController::Ins()->index($request);
});