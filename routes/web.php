<?php

use \Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "Welcome to message center ! - " . "[" . date('Y/m/m H:i:s') . "]";
    //return view('welcome');
});

// Pages 业务路由组
Route::group([ 'prefix' => '/pages'], function () {
    // 需要签名验证的页面
    Route::group([
        //'middleware' => 'pages',
    ], function () {
        // 后台推送统计页面
        Route::get('/madmin/push/statistic', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Pages\MadminController::Ins()->statistic($request);
        });
        // 后台推送列表页面
        Route::get('/madmin/push/searchList', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Pages\MadminController::Ins()->searchList($request);
        });
        // 后台推送配置页面
        Route::get('/madmin/push/setting', function (\Illuminate\Http\Request $request) {
            return \App\Http\Controllers\Pages\MadminController::Ins()->setting($request);
        });
    });
});

// Artisan 路由组
Route::group(['prefix' => '/artisan'], function () {
    // rabbit mq
    Route::get('/rabbitmq', function (\Illuminate\Http\Request $request) {
        return \App\Http\Controllers\Commands\RabbitMQController::Ins()->run($request);
    });
    // command
    Route::get('/command', function (\Illuminate\Http\Request $request) {
        return \App\Http\Controllers\Commands\CommandController::Ins()->run($request);
    });
});
