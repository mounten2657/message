

// 初始化时间控件
function initDateTime(startId, endId) {
    $(startId).datetimepicker({
        keyboardNavigation: false,
        language: 'zh-CN',
        forceParse: false,
        autoclose: true,
        todayBtn: true,
        format: 'yyyy-mm-dd hh:ii:00',
        //startDate: new Date()
    }).on("click", function () {
        $(startId).datetimepicker("setEndDate", $(endId).val())
    });
    $(endId).datetimepicker({
        keyboardNavigation: false,
        language: 'zh-CN',
        forceParse: false,
        autoclose: true,
        todayBtn: true,
        format: 'yyyy-mm-dd hh:ii:00',
        startDate: new Date()
    }).on("click", function () {
        $(endId).datetimepicker("setStartDate", $(startId).val())
    });
}

// 生成参数签名
function getSign(params, scene = 'pages/', platform = 'MADMIN') {
    params.scene = scene;
    params.platform = platform;
    params.timestamp = parseInt(Date.parse(new Date())/1000);
    params.keys = getUrlInput('keys');
    params.signType = 'md5';
    if (!params.order) params.order = '';
    if (!params.sort) params.sort = '';
    if (!params.search) params.search = '';
    let keys  = Object.keys(params).sort();
    let sortStr = '';
    for (let i = 0 ; i < keys.length; i ++) {
        sortStr += keys[i] + '=' + params[keys[i]] + '&';
    }
    sortStr = sortStr.replace(/[\r\n\s]/g,"");
    sortStr += '&key=i^!$K*ttZS@eDOT9dEmK^xs*z4pVozBQGos';
    params.sign = md5(sortStr).toUpperCase();
    return params;
}

// 获取链接参数
function getUrlInput(variable, defaultVal = '')
{
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] === variable){return pair[1];}
    }
    return defaultVal;
}

// 转换时间戳转换为datetime格式
function datetimeFormat(timestamp) {
    var dateVal = timestamp + "";
    if (timestamp != null) {
        var date = new Date(parseInt(dateVal.replace("/Date(", "").replace(")/", ""), 10) * 1000);
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        return date.getFullYear() + "-" + month + "-" + currentDate + " " + hours + ":" + minutes + ":" + seconds;
    }
}

