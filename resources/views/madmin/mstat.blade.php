<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Message Push Center</title>
    <!-- 引入 bootstrap.css -->
    <link rel="stylesheet" href="{{config('app.url')}}/asset/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{config('app.url')}}/asset/bootstrap/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="{{config('app.url')}}/asset/bootstrap/css/bootstrap-table.min.css">
    <!-- 引入 echarts.js -->
    <script src="{{config('app.url')}}/asset/echarts.min.js"></script>
    <!-- 引入 jquery.js -->
    <script src="{{config('app.url')}}/asset/jquery.min.js"></script>
    <!-- 引入 bootstrap.js -->
    <script src="{{config('app.url')}}/asset/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{config('app.url')}}/asset/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{config('app.url')}}/asset/bootstrap/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="{{config('app.url')}}/asset/bootstrap/js/bootstrap-table.min.js"></script>
    <script src="{{config('app.url')}}/asset/bootstrap/js/locales/bootstrap-table-zh-CN.js"></script>
    <!-- 引入 layer.js -->
    <script src="{{config('app.url')}}/asset/layer/layer.js"></script>
    <script src="{{config('app.url')}}/asset/md5.js"></script>
    <script src="{{config('app.url')}}/asset/functions.js"></script>
    <style type="text/css">.table {table-layout:fixed;word-break:break-all;}</style>
</head>
<body>

<input type="hidden" id="baseUrl" value="{{config('app.url')}}">
<div class="container">

    <h3>消息推送中心 -> 推送统计</h3>
    <hr>
    <div class="row">
        <div class="row">
            <div class="col-md-3 text-center">
                <label>消息渠道：</label>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <span class="buttonText">-- 请选择</span>
                        <input type="hidden" id="channelId" value="0" >
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" id="channel">
                        <li><a href="#" onclick="shows($(this).text(), 2)">钉钉推送</a></li>
                        <li><a href="#" onclick="shows($(this).text(), 1)">短信推送</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="form-group">
                        <div class="control-label form-inline">
                            <label>统计日期：</label>
                            <input type="text" class="form-control" id="start_time" placeholder="开始时间" style="width: 180px;"/>
                            <input type="text" class="form-control" id="end_time" placeholder="结束时间" style="width: 180px;margin-right: 20px;"/>
                            <button type="button" class="btn btn-info" id="btn_query">查询</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">

            </div>
        </div>
        <hr>
        <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
        <div id="main" style="width:1200px;height:600px;"></div>
    </div>

</div>

<script type="text/javascript">

    let baseUrl = $('#baseUrl').val();

    $(function () {

        // 初始化时间
        initDateTime('#start_time', '#end_time');

        // 初始化数据图表
        let myChart = echarts.init(document.getElementById('main'));
        refreshChart(myChart);

        // 查询按钮点击
        $("#btn_query").on('click', function () {
            changeData(myChart);
        });

        // 下拉框改变
        $('#channel').on('click', function () {
            changeData(myChart);
        });

    });

    function shows(text, value) {
        $('.buttonText').text(text);
        $('#channelId').val(value);
    }

    function changeData(chart) {
        let params = {};
        let start_time = $('#start_time').val();
        let end_time = $('#end_time').val();
        let channel_id = $('#channelId').val();
        if (start_time > end_time) {
            layer.msg('结束时间不能小于开始时间');
            return false;
        }
        params.channel_id = channel_id;
        if (start_time && end_time) {
            params.start_time = start_time;
            params.end_time = end_time;
        }
        chart.dispose();
        chart = echarts.init(document.getElementById('main'));
        refreshChart(chart, params);
    }

    function refreshChart(chart, params = {}) {
        // 异步加载数据
        chart.showLoading();
        $.ajax({
            url: baseUrl + '/api/statistic/data',
            method: 'post',
            data: getSign(params, 'pages/mstat'),
            success: function (res) {
                if (res.status === 'success') {
                    let option = {};
                    if (!params.channel_id) {
                        option = getOptionData(res.result);
                    } else {
                        option = getChannelOptionData(res.result);
                    }
                    chart.hideLoading();
                    chart.setOption(option);
                }
            },
            error: function (res) {
                chart.hideLoading();
                layer.msg('服务器错误，请重试');
                console.log(res.responseText)
            }
        });
    }

    // 获取图表数据配置
    function getOptionData(data) {
        return {
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b}: {c} ({d}%)'
            },
            legend: {
                orient: 'vertical',
                left: 10,
                data: data.all_type
            },
            series: [
                {
                    name: '推送次数',
                    type: 'pie',
                    selectedMode: 'single',
                    radius: [0, '30%'],
                    top: 20,
                    left: 100,
                    label: {
                        position: 'inner'
                    },
                    labelLine: {
                        show: false
                    },
                    data: data.first_type
                },
                {
                    name: '推送次数',
                    type: 'pie',
                    radius: ['40%', '55%'],
                    top: 20,
                    left: 100,
                    label: {
                        formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c}  {per|{d}%}  ',
                        backgroundColor: '#eee',
                        borderColor: '#aaa',
                        borderWidth: 1,
                        borderRadius: 4,
                        rich: {
                            a: {
                                color: '#999',
                                lineHeight: 22,
                                align: 'center'
                            },
                            abg: {
                                backgroundColor: '#444',
                                width: '100%',
                                align: 'right',
                                height: 22,
                                borderRadius: [4, 4, 0, 0]
                            },
                            hr: {
                                borderColor: '#aaa',
                                width: '100%',
                                borderWidth: 0.5,
                                height: 0
                            },
                            b: {
                                fontSize: 16,
                                lineHeight: 33
                            },
                            per: {
                                color: '#eee',
                                backgroundColor: '#334455',
                                padding: [2, 4],
                                borderRadius: 2
                            }
                        }
                    },
                    data: data.rows
                }
            ]
        };
    }

    // 获取固定渠道数据
    function getChannelOptionData(data) {
        return {
            title: {
                text: '',
                subtext: '',
                left: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b} : {c} ({d}%)'
            },
            legend: {
                type: 'scroll',
                orient: 'vertical',
                right: 10,
                top: 20,
                bottom: 20,
                data: data.all_type,
                //selected: data.selected
            },
            series: [
                {
                    name: '类型',
                    type: 'pie',
                    radius: '55%',
                    center: ['40%', '50%'],
                    data: data.rows,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
    }

</script>
</body>
</html>