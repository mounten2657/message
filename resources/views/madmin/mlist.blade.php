<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Message Push Center</title>
    <!-- 引入 bootstrap.css -->
    <link rel="stylesheet" href="{{config('app.url')}}/asset/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{config('app.url')}}/asset/bootstrap/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="{{config('app.url')}}/asset/bootstrap/css/bootstrap-table.min.css">
    <!-- 引入 echarts.js -->
    <script src="{{config('app.url')}}/asset/echarts.min.js"></script>
    <!-- 引入 jquery.js -->
    <script src="{{config('app.url')}}/asset/jquery.min.js"></script>
    <!-- 引入 bootstrap.js -->
    <script src="{{config('app.url')}}/asset/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{config('app.url')}}/asset/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{config('app.url')}}/asset/bootstrap/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="{{config('app.url')}}/asset/bootstrap/js/bootstrap-table.min.js"></script>
    <script src="{{config('app.url')}}/asset/bootstrap/js/locales/bootstrap-table-zh-CN.js"></script>
    <!-- 引入 layer.js -->
    <script src="{{config('app.url')}}/asset/layer/layer.js"></script>
    <script src="{{config('app.url')}}/asset/md5.js"></script>
    <script src="{{config('app.url')}}/asset/functions.js"></script>
    <style type="text/css">.table {table-layout:fixed;word-break:break-all;}</style>
</head>
<body>

<input type="hidden" id="baseUrl" value="{{config('app.url')}}">
<div class="container_x" style="padding: 0px 50px">

    <h3>消息推送中心 -> 推送列表</h3>
    <table id="table"></table>

    <!-- 表格搜索栏 -->
    <form class="form-inline " id="toolbar">
        <label>搜索条件：</label>
        <select class="form-control" id="channelId">
            <option value="0">渠道</option>
            <option value="1">短信</option>
            <option value="2">钉钉</option>
        </select>
        <!-- 需要二级联动菜单 -->
        <select class="form-control" id="msgType">
            <option value="0">消息类型</option>
            <option value="0">|--短信</option>
            <option value="1">----验证码</option>
            <option value="2">----课堂报告</option>
            <option value="3">----缴费通知</option>
            <option value="4">----订单通知</option>
            <option value="5">----校长日报</option>
            <option value="6">----老师消课提醒</option>
            <option value="0">|--钉钉</option>
            <option value="101">----经营日报</option>
            <option value="11">----教师消课提醒</option>
            <option value="12">----校长今日简报</option>
            <option value="13">----产品昨日使用简报</option>
            <option value="14">----领导-流水排行</option>
            <option value="15">----领导-指标完成</option>
            <option value="16">----系统公告</option>
            <option value="17">----招聘数据提醒</option>
            <option value="18">----学管师课前提醒</option>
            <option value="29">----教师明日课表提醒</option>
            <option value="20">----家长反馈日报-校长</option>
            <option value="21">----家长反馈日报-分公司</option>
            <option value="22">----家长反馈周报-校区</option>
            <option value="23">----家长反馈周报-分公司</option>
            <option value="24">----失效课程通知</option>
            <option value="25">----提点扣罚确认通知</option>
            <option value="26">----诺约通知-咨询个人</option>
            <option value="27">----诺约通知-校长-主管</option>
            <option value="28">----诺约通知-区域经理</option>
            <option value="29">----诺约通知-分公司经理</option>
            <option value="30">----学员关注排行</option>
            <option value="31">----行政预约通知</option>
            <option value="0">|--其他</option>
            <option value="999">----其他</option>
        </select>
        <input type="text" class="form-control" id="mobile" placeholder="手机号码" style="width: 120px;">
        <input type="text" class="form-control" id="realname" placeholder="真实姓名" style="width: 120px;">
        <input type="text" class="form-control" id="start" placeholder="开始时间" style="width: 180px;"/>
        <input type="text" class="form-control" id="end" placeholder="结束时间" style="width: 180px;"/>
        <button type="button" id="btn_query" class="btn btn-secondary mb-2" title="搜索"><i class="glyphicon glyphicon-search" aria-hidden="true"></i></button>
    </form>

</div>

<script type="text/javascript">

    let baseUrl = $('#baseUrl').val();

    $(function () {

        // 初始化时间
        initDateTime('#start', '#end');

        // 初始化列表数据
        let table = $('#table');
        initTable(table);

        // 点击搜索请求数据列表
        $("#btn_query").on('click', function () {
            //点击查询是 使用刷新 处理刷新参数
            table.bootstrapTable('refresh');
        });

    });

    // 初始化表格数据
    function initTable(table, data = []) {
        // 先销毁再创建
        table.bootstrapTable('destroy').bootstrapTable({
            //data: data, // 本地数据列表
            url: baseUrl + "/api/search/list",
            method: 'post',
            dataType: "json",
            contentType: "application/x-www-form-urlencoded",
            classes: 'table table-bordered table-hover table-striped', // 样式，table-striped 隔行变色
            pagination: true,//是否分页
            pageNumber : 1, //初始化加载第一页
            pageSize: 10,//单页记录数
            pageList: [5, 10, 15, 20],//分页步进值
            sidePagination: "server",//服务端分页
            //search: true,//本地数据表格搜索，配合data使用
            showRefresh: true,//刷新按钮
            showColumns: true,//列选择按钮
            toolbar: '#toolbar',
            queryParamsType: "limit",//查询参数组织方式
            queryParams : function(params) {//上传服务器的参数
                return getSearchParams(params);
            },
            columns: [ // 列详情
                {
                    title: 'ID',// 表头字段名
                    field: 'id',// 数据 key
                    sortable: true,// 排序
                    align: 'center',// 居中
                    visible: true
                }, {
                    title: '渠道',
                    field: 'channel',
                    align: 'center'
                }, {
                    title: '消息类型',
                    field: 'msg_type',
                    width:  80,
                    align: 'center'
                }, {
                    title: '用户名',
                    field: 'realname',
                    align: 'center'
                }, {
                    title: '手机号',
                    field: 'mobile',
                    width:  110,
                    align: 'center'
                }, {
                    title: '推送内容',
                    field: 'body',
                    width:  360,
                    align: 'center'
                }, {
                    title: '角色',
                    field: 'role',
                    align: 'center'
                }, {
                    title: '城市',
                    field: 'city',
                    align: 'center',
                    visible: false
                }, {
                    title: '校区',
                    field: 'campus',
                    align: 'center'
                }, {
                    title: '结果',
                    field: 'result',
                    align: 'center',
                    visible: false
                }, {
                    title: '状态',
                    field: 'status',
                    align: 'center'
                }, {
                    title: '备注',
                    field: 'remark',
                    align: 'center',
                }, {
                    title: '创建时间',
                    field: 'create_time',
                    width:  90,
                    align: 'center'
                }

            ]
        });
    }

    // 获取搜索数据
    function getSearchParams(params) {
        // 默认参数
        params.page = (params.offset / params.limit) + 1; //当前页码

        // 接收参数
        let channelId = $('#channelId').val();
        let msgType = $('#msgType').val();
        let mobile = $('#mobile').val();
        let realname = $('#realname').val();
        let start_time = $('#start').val();
        let end_time = $('#end').val();

        if ('0' !== channelId) params.channel_id = channelId;
        if ('0' !== msgType) params.msg_type = msgType;
        if (mobile) params.mobile = mobile;
        if (realname) params.realname = realname;

        if (start_time > end_time) {
            layer.msg('结束时间不能小于开始时间');
            return false;
        }
        if (start_time) params.start_time = start_time;
        if (end_time) params.end_time = end_time;

        params = getSign(params, 'pages/mlist');

        return params;
    }

</script>
</body>
</html>