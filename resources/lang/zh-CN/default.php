<?php

/*
|--------------------------------------------------------------------------
| Default Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during authentication for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/
return [
    // 自定义
    'Login error' => '请登录后进行操作',
    'Sign error' => '签名校验失败',
    'These credentials do not match our records.' => '证书错误',
    'Too many login attempts, Please try again later.' => '登录操作频繁，请稍后再试',
    'Success' => '操作成功',
    'Failed' => '操作失败',
    'Server Error' => '服务器异常，请稍后重试',
    'Empty Record' => '未查询到有效信息',
    'SQL Error' => '系统错误，请稍后重试',
    'Code disable' => '该验证码已经失效，请重新获取',
    'User status is disabled' => '您的账号存在异常情况，如有疑问请与我们联系',
    'User not exists' => '账号不存在',
    'Save failed' => '保存失败，请稍后重试',
    'User have not phone' => '请绑定手机号',
    'Sms send failed' => '短信发送失败',
    'Sms has been send' => '短信发送频繁，请稍后重试',
    'Unauthorized' => '操作失败',
    'Upload file fail' => '因网络原因上传文件失败，请重试',
    'Unstable network' => '网络不稳定，请重试',
    'Missing Params' => '缺少必要参数',
    'Illegal Params' => '参数不合法',
    'Ding message send success' => '钉钉消息发送成功',
    'Sms message send success' => '验证码获取成功',
    'Sms code verified' => '验证码验证通过',
    'Sms code verify failed' => '验证码验证失败',
    'Add by api' => '由接口自动添加',
    'Push to rmq failed' => '入队列失败，请稍后重试',
    'Update task status failed' => '更新任务状态失败，请稍后重试',
    'Push success' => '第三方推送成功',
    'Push failed' => '第三方推送失败',
    'No such a task' => '该推送任务不存在或已失效',
    'Page expired' => '页面已过期',
    'Queue reloading' => '队列重启中，可能需要一到三分钟时间',
    'Send success' => '发送成功',
    'Lower Version' => '您的版本过低，请及时升级开放接口',
    'Scene Closed' => '非常抱歉，该开放接口已关闭',
    'Key Invalid' => '您的Key值无效，请联系开放平台',
    'Scene Channel Invalid' => '开放接口不在白名单内',
];
