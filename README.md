
### Laravel v5.4 for message center

#### 消息推送平台说明

 - 平台下的接口均是走异步操作，外部请求消息平台接口，可以看做一个publisher，会先将数据上传到rabbitmq，逻辑功能在consumer中实现，所有接口均会有详细的日志记录；
 - 平台集成了短信发送和钉钉消息推送，外部无需再写对应插件，只需传参直接请求http接口即可，如果长时间没有收到推送消息，注意在用户端应有对应的toast提示用户重试操作；
 - 平台提供了一定的可视化图表和消息推送记录搜索功能，可在madmin中查看。
 
#### 外部请求平台接口公共参数
  - timestamp : 当前时间戳，单位秒
  - sign : 参数签名
  - platform ： 当前应用平台。
  - scene ：当前应用场景值。
 
目前已知的平台`platform`列表如下（持续更新）：
    - CITY
	- CAMPUS
	- IT  

目前已知的短信场景`scene`值如下（持续更新）：	
    - 登录发送短信验证码	

目前已知的钉钉场景`scene`值如下（持续更新）：	
    - 分公司日报钉钉推送
	
#### 参数签名生成规则
 
```
  Step1: 设所有发送或者接收到的数据为集合M，将集合M内非空参数值的参数按照参数名ASCII码从小到大排序（字典序），使用URL键值对的格式（即key1=value1&key2=value2…）拼接成字符串stringA。
	特别注意以下重要规则：
	◆ 参数名ASCII码从小到大排序（字典序）；
	◆ 如果参数的值为空不参与签名；
	◆ 参数名区分大小写；
	◆ 验证调用返回或微信主动通知签名时，传送的sign参数不参与签名，将生成的签名与该sign值作校验。
	◆ 微信接口可能增加字段，验证签名时必须支持增加的扩展字段
   Step2: 在stringA最后拼接上key得到stringSignTemp字符串，并对stringSignTemp进行MD5运算，再将得到的字符串所有字符转换为大写，得到sign值signValue。 注意：密钥的长度为32个字节。
    ◆ key获取途径：key为消息平台秘钥，直接联系对应开发人员获取。
```
```
举例：
假设传送的参数如下：
	appid： wxd930ea5d5a258f4f
	mch_id： 10000100
	device_info： 1000
	body： test
	nonce_str： ibuaiVcKdpRxkhJA
第一步：对参数按照key=value的格式，并按照参数名ASCII字典序排序如下：
	stringA="appid=wxd930ea5d5a258f4f&body=test&device_info=1000&mch_id=10000100&nonce_str=ibuaiVcKdpRxkhJA";
第二步：拼接API密钥：
	stringSignTemp=stringA+"&key=192006250b4c09247ec02edce69f6a2d" //注：key为消息平台设置的密钥key
	sign=MD5(stringSignTemp).toUpperCase()="9A0A8659F005D6984697E2CA0A9CF3B7" //注：MD5签名方式
最终得到最终发送的数据（以json格式为例）：
{
	"appid": "wxd930ea5d5a258f4f",
	"mch_id": "10000100",
	"device_info": "1000",
	"body": "test",
	"nonce_str": "ibuaiVcKdpRxkhJA",
	"sign": "9A0A8659F005D6984697E2CA0A9CF3B7"
}
```



