<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

/**
 * 数据统计接口
 * Class StatController
 * @package App\Http\Controllers\Admin
 */
class StatController extends BaseController
{

    /**
     * @var array 参数验证器
     */
    public $validate = [
        "getData" => [
            "rules" => [
                "channel_id" => "int|max:20",
                "start_time" => "string|date",
                "end_time" => "string|date",
            ]
        ],
    ];

    /**
     * 获取统计数据，用于图形展示
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData(Request $request)
    {
        // 接收参数
        $params = $request->all();
        // 调起服务
        $res = service()->MsgSendService->getStatisticData($params);
        // 结果返回
        return $this->apiReturn($res);
    }

}