<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

/**
 * 推送设置接口
 * Class StatController
 * @package App\Http\Controllers\Admin
 */
class SettingController extends BaseController
{

    /**
     * @var array 参数验证器
     */
    public $validate = [
        "getList" => [
            "rules" => [
                "scene_type" => "required|string",
                "offset" => "int",
                "limit" => "int",
            ]
        ],
        "save" => [
            "rules" => [
                "scene_id" => "required|int",
                "scene_type" => "required|string",
                "scene_name" => "string|max:1000",
                "remark" => "string|max:1000",
                "msg_type" => "int",
                "status" => "int",
                "body" => "string|max:1000",
                "scene_key" => "int",
                "channel_id" => "int",
                "platform_id" => "int",
                "refresh_key" => "int",
            ]
        ],
        "queue" => ["rules" => []],
        "queueReload" => ["rules" => []],
        "info" => ["rules" => []],
        "getTypeList" => [
            "rules" => [
                "scene_type" => "required|string",
            ]
        ],
    ];

    /**
     * 获取场景列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $params = $request->all();
        $res = service()->MsgSceneService->getDetailList($params);
        if (isset($res['code'])) {
            return $this->apiReturn($res['data'], $res['msg'], $res['code']);
        }
        return $this->listReturn($res);
    }

    /**
     * 更新场景配置
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $params = $request->all();
        $res = service()->MsgSceneService->updateSceneInfo($params);
        if (isset($res['code'])) {
            return $this->apiReturn($res['data'], $res['msg'], $res['code']);
        }
        return $this->apiReturn($res);
    }

    /**
     * 获取队列信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function queue(Request $request)
    {
        $params = $request->all();
        $res = service()->MsgSceneService->getQueueInfo($params);
        if (isset($res['code'])) {
            return $this->apiReturn($res['data'], $res['msg'], $res['code']);
        }
        return $this->apiReturn($res);
    }

    /**
     * 重启队列
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function queueReload(Request $request)
    {
        $params = $request->all();
        // send http response first.
        response()->json($this->buildJson([], lang('Queue reloading')))->send();
        service()->MsgSceneService->queueReload($params);
        return '';
    }

    /**
     * 获取系统信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function info(Request $request)
    {
        $params = $request->all();
        $res = service()->MsgSceneService->getSystemInfo($params);
        if (isset($res['code'])) {
            return $this->apiReturn($res['data'], $res['msg'], $res['code']);
        }
        return $this->apiReturn($res);
    }

    /**
     * get type list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTypeList(Request $request)
    {
        $params = $request->all();
        $type = getVal($params, 'scene_type');
        $res = service()->MsgSceneService->getTypeList($type);
        if (isset($res['code'])) {
            return $this->apiReturn($res['data'], $res['msg'], $res['code']);
        }
        return $this->apiReturn($res);
    }


}