<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

/**
 * 数据搜索接口
 * Class StatController
 * @package App\Http\Controllers\Admin
 */
class SearchController extends BaseController
{

    /**
     * @var array 参数验证器
     */
    public $validate = [
        "getList" => [
            "rules" => [
                "channel_id" => "int",
                "scene_id" => "int",
                "mobile" => "phone",
                "realname" => "string|max:100",
                "start_time" => "date",
                "end_time" => "date",
            ]
        ],
        "getTypeList" => [
            "rules" => [
                "type" => "string|max:24",
            ]
        ],
    ];

    /**
     * 获取消息推送列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        // 接收参数
        $params = $request->all();
        // 调起服务
        $res = service()->MsgSendService->getSearchData($params);
        // 列表返回
        return $this->listReturn($res);
    }

    /**
     * 获取消息推送类型列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTypeList(Request $request)
    {
        // 接收参数
        $params = $request->all();
        $type = !empty($params['type']) ? $params['type'] : '';
        // 调起服务
        $res = service()->MsgSendService->getTypeList($type);
        // 结果返回
        return $this->apiReturn($res);
    }

}