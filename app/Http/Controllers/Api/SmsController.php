<?php

namespace App\Http\Controllers\Api;

use App\Consts\MessageConst;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class SmsController extends BaseController
{

    /**
     * @var array 参数验证器
     */
    public $validate = [
        "send" => [
            "rules" => [
                "mobile" => "required|phone",
                "platform" => "required|max:1000",
                "scene" => "required|max:1000",
            ]
        ],
        "verifyCode" => [
            "rules" => [
                "task_id" => "required|string|max:24",
                "mobile" => "required|phone",
                "code" => "required|max:6",
            ]
        ],
        "union" => [
            "rules" => [
                "mobile" => "required|phone",
                "project_id" => "required|string|max:1000",
                "sms_type" => "required|int|max:10",
                "var_json" => "required|json",
                "platform" => "required|max:1000",
                "scene" => "required|max:1000",
            ]
        ],
    ];

    /**
     * 发送短信验证码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $request)
    {
        // 接收参数
        $params = $request->all();
        $params['channel_id'] = MessageConst::CHANNEL_SMS_ID;
        $params['project_id'] = $request->get('project_id', '');
        // 调起服务
        $res = service()->SmsService->sendSmsMsg($params);
        // 结果返回
        return $this->apiReturn($res, lang('Sms message send success'));
    }

    /**
     * 验证短信验证码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyCode(Request $request)
    {
        // 接收参数
        $params = $request->all();
        // 调起服务
        $res = service()->SmsService->verifyCode($params['task_id'], $params['mobile'], $params['code']);
        if (!$res) {
            return $this->errorReturn(lang('Sms code verify failed'));
        }
        // 结果返回
        return $this->apiReturn($res, lang('Sms code verified'));
    }

    /**
     * 发送短信通知（全类型）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function union(Request $request)
    {
        // 接收参数
        $params = $request->all();
        $params['channel_id'] = MessageConst::CHANNEL_SMS_ID;
        // 调起服务
        $res = service()->SmsService->sendSmsMsg($params);
        // 结果返回
        return $this->apiReturn($res, lang('Sms message send success'));
    }

}