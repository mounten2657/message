<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class TaskController extends BaseController
{

    /**
     * @var array 参数验证器
     */
    public $validate = [
        "getStatus" => [
            "rules" => [
                "task_id" => "required|string|max:24",
            ]
        ],
    ];

    /**
     * 获取推送状态
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatus(Request $request)
    {
        // 接收参数
        $params = $request->all();
        // 调起服务
        $res = service()->MsgSendService->getTaskStatus($params['task_id']);
        // 结果返回
        return $this->apiReturn($res);
    }

}