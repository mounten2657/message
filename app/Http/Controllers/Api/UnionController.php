<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

/**
 * 整合 - 统一发送接口
 * Class UnionController
 * @package App\Http\Controllers\Api
 */
class UnionController extends BaseController
{

    /**
     * @var array 参数验证器
     */
    public $validate = [
        "send" => [
            "rules" => [
                "user_list" => "required",
                "content_json" => "required|json",
                "scene_key" => "required|string|max:32",
            ]
        ]
    ];

    /**
     * 统一发送接口
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $request)
    {
        // 接收参数
        $params = $request->all();
        // 调起服务
        $res = service()->UnionService->sendMsg($params);
        if (isset($res['code'])) {
            return $this->apiReturn($res['data'], $res['msg'], $res['code']);
        }
        // 返回结果
        return $this->apiReturn($res, lang('Send success'));
    }

}