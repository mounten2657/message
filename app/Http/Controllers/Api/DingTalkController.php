<?php

namespace App\Http\Controllers\Api;

use App\Consts\MessageConst;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class DingTalkController extends BaseController
{

    /**
     * @var array 参数验证器
     */
    public $validate = [
        "send" => [
            "rules" => [
                "mobile_list" => "required_without:userid_list",
                "userid_list" => "required_without:mobile_list",
                "msg" => "required|json",
                "msg_type" => "required|integer|max:10000",
                "platform" => "required|max:1000",
                "scene" => "required|max:1000",
            ]
        ],
    ];

    /**
     * 发送钉钉消息通知
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @response {"status":"success","code":2000,"msg":"\u9489\u9489\u6d88\u606f\u53d1\u9001\u6210\u529f","result":{"task_id":"00KOTGSSHVTNMU6EUPNH7QKU","rep":true}}
     */
    public function send(Request $request)
    {
        // 接收参数
        $params = $request->all();
        $params['channel_id'] = MessageConst::CHANNEL_DINGTALK_ID;
        // 调起服务
        $res = service()->DingService->sendDingMsg($params);
        // 结果返回
        return $this->apiReturn($res, lang('Ding message send success'));
    }

}