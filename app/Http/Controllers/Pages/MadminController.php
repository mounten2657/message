<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

/**
 * 后台页面
 * Class MadminController
 * @package App\Http\Controllers\Api
 */
class MadminController extends BaseController
{

    /**
     * 统计页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statistic(Request $request)
    {
        return view('madmin.mstat');
    }

    /**
     * 列表页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchList(Request $request)
    {
        return view('madmin.mlist');
    }

    /**
     * 配置页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function setting(Request $request)
    {
        return view('madmin.mset');
    }


}