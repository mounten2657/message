<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AllowOrigin
{

    /**
     *
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        header('Access-Control-Allow-Origin: *');
        return $next($request);
    }

}