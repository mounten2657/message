<?php

namespace App\Models;

/**
 * App\Models\MsgScene
 *
 * @property int $id 主键ID
 * @property string $name 消息来源名称，自定义
 * @property int $platform_id 消息平台ID，关联 nice_msg_platform.id
 * @property int $channel_id 消息渠道ID，关联 nice_msg_channel.id
 * @property int $status 状态：1代表正常
 * @property string $remark 备注
 * @property int $is_deleted 是否被删除：0否，1是
 * @property int $create_time 创建时间
 * @property int $update_time 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgScene whereChannelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgScene whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgScene whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgScene whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgScene whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgScene wherePlatformId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgScene whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgScene whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgScene whereUpdateTime($value)
 * @mixin \Eloquent
 */
class MsgScene extends BaseModel
{

    protected $table = "msg_scene";
    protected $primaryKey = "id";
    protected $keyType = "int";

    /**
     * 关联模型 - platform
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function platform()
    {
        return $this->hasOne(MsgPlatform::class, 'id', 'platform_id');
    }

    /**
     * 关联模型 - channel
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function channel()
    {
        return $this->hasOne(MsgChannel::class, 'id', 'channel_id');
    }

}