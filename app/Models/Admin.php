<?php

namespace App\Models;

/**
 * App\Models\Admin
 * This is the model class for table "admin".
 *
 * @property int $id 自增ID
 * @property int $creator_id 创建者ID
 * @property string $name 管理员名称
 * @property string $username 登录账户名
 * @property string $password_hash 密码
 * @property int $status 管理员状态，0禁用，1启用
 * @property int $is_deleted 是否删除，0正常，1删除
 * @property int $created_at 创建时间戳
 * @property int $updated_at 更新时间戳
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin query()
 */
class Admin extends BaseModel
{

    protected $table = "admin";
    protected $primaryKey = "id";
    protected $keyType = "int";
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    

}
