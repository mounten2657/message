<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 *
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder select($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel getQuery()
 * @mixin \Eloquent
 */
class BaseModel extends Model
{

    /** @var array $fields */
    public static $fields = [];

    /** @var string $dateFormat */
    protected $dateFormat = 'U';

    /** @var array $guarded */
    protected $guarded = [];

    /** @var $instance */
    public static $instance;

    /** @var $className */
    public static $className;

    /**
     * set failed format
     * @example ['updated_at' => 'timestamp', 'created_at' => 'timestamp']
     * @preview integer | real | float | double | decimal:<digits> | string | boolean | object | array | collection | timestamp | date | datetime
     * @var array $casts
     */
    protected $casts = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::$className = static::class;
    }

    /**
     * get model instance
     * @return static
     */
    public static function getInstance()
    {
        if (!empty(self::$instance[self::$className])) {
            return self::$instance[self::$className];
        }
        return self::$instance[self::$className] = new static;
    }

}
