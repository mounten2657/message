<?php

namespace App\Models;

/**
 * App\Models\EduAdmin
 *
 * @property int $userid 用户ID
 * @property int $comid 分公司
 * @property string|null $username 账户登录名
 * @property string|null $realname 真实姓名
 * @property string|null $letter 首字母
 * @property string|null $encrypt 秘钥
 * @property string|null $passwd 密码
 * @property string|null $mycode 邀请码
 * @property string|null $avatar 用户头像
 * @property int $gender 性别
 * @property string|null $mobile 手机号码
 * @property string|null $oicq QQ号码
 * @property string|null $weixin 微信
 * @property string|null $email Email
 * @property int $role 角色
 * @property string|null $level 权限体系
 * @property int $status 账户状态
 * @property int $loginnum 登录次数
 * @property int $validity 过期时间
 * @property int $ctime 入库时间
 * @property int $cuid 添加者
 * @property int $mtime 修改时间
 * @property int $muid 修改者
 * @property int $city 分公司
 * @property int $campus 校区
 * @property string|null $m_subject 授课科目
 * @property string|null $m_campus 授课校区
 * @property int $is_leader 是否为主管
 * @property int $is_sanshi 是否是三师老师
 * @property int $is_part 是否是兼职
 * @property string $id_card 身份证号
 * @property int $entry_time 入职时间
 * @property int $kb_color 学管师课表颜色
 * @property string $token token值，API接口身份唯一识别
 * @property int $token_expired_time token失效时间
 * @property string|null $level_menu 权限菜单
 * @property string|null $level_new 权限功能URL
 * @property int $is_employee 是否是员工，1是正式员工，2虚拟账号
 * @property int $is_research 是否教研（1是，2不是）仅教师账号有效
 * @property string $position 岗位
 * @property int $member_status 在职状态  1 在职  2 离职
 * @property string $birthday 生日
 * @property int $marry 婚姻状况  2 已婚  1 未婚   3离异
 * @property int $political_status 政治面貌   3党员   2团员  1群众
 * @property int $is_formal 是否转正   1  是   2否
 * @property int $formal_time 转正时间
 * @property int $job_age 司龄  (月份)
 * @property int $contract_starttime 合同起始时间
 * @property int $contract_endtime 合同结束时间
 * @property int $account_type 户籍类型  1本地城镇  2本地农村  3外地城镇  4外地农村
 * @property string $account_addr 户口所在地
 * @property string $mailing_address 通信地址
 * @property string $emergency_contact 紧急联系人
 * @property string $emergency_call 紧急联系电话
 * @property int $education 学历  1高中以下  2高中  3专科  4本科 5研究生
 * @property int $educational_level 学位   1学士  2硕士   3博士  4其它
 * @property int $graduate_time 毕业时间
 * @property string $school 毕业学校
 * @property string $vocational 所学专业
 * @property string $certificate 专业技术资格证
 * @property string $salary_card 中国银行卡号(薪资卡)
 * @property int $department 部门
 * @property int $t_role t_上的role
 * @property int $t_userid t_admin表主键
 * @property string|null $city_campus_level city_campus权限：
 * @property string|null $nice_ding_userid 钉钉userid
 * @property string $ding_mobile 钉钉帐号
 * @property string $cx_ding_userid 锤星钉钉userid
 * @property int $classin_uid classin平台上人用户ID
 * @property string $call_800_username 八百呼账号
 * @property string $clal_800_passwd 八百呼密码
 * @property string $jobnumber
 * @property int $is_release 是否脱产   1是  2否
 * @property int $orderlist 排序
 * @property string|null $job_responsibilities 工作职责
 * @property string $real_mobile 用户真实号码
 * @property int $is_test_username 是否测试专用帐号
 * @property string $yr_id 云容ID
 * @property mixed|null $yr_face 云容人脸信息
 * @property int $area 区域
 * @property int|null $subject_group 科目分组
 * @property int $resign_ymd 离职时间
 * @property int $position_level 职级
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereAccountAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereAccountType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereCall800Username($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereCampus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereCityCampusLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereClal800Passwd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereClassinUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereComid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereContractEndtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereContractStarttime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereCtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereCuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereCxDingUserid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereDingMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereEducationalLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereEmergencyCall($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereEmergencyContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereEncrypt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereEntryTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereFormalTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereGraduateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereIdCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereIsEmployee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereIsFormal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereIsLeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereIsPart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereIsRelease($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereIsResearch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereIsSanshi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereIsTestUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereJobAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereJobResponsibilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereJobnumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereKbColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereLetter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereLevelMenu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereLevelNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereLoginnum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereMCampus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereMSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereMailingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereMarry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereMemberStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereMtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereMuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereMycode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereNiceDingUserid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereOicq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereOrderlist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin wherePasswd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin wherePoliticalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin wherePositionLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereRealMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereRealname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereResignYmd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereSalaryCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereSchool($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereSubjectGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereTRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereTUserid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereTokenExpiredTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereUserid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereValidity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereVocational($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereWeixin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereYrFace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduAdmin whereYrId($value)
 * @mixin \Eloquent
 */
class EduAdmin extends BaseModel
{

    protected $table = "admin";
    protected $primaryKey = "userid";
    protected $keyType = "int";
    public $connection = 'pgsql-edu';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    

}
