<?php

namespace App\Models;

/**
 * App\Models\MsgConfig
 *
 * @property int $id 主键ID
 * @property string $item 配置项名称
 * @property string $value 配置项值
 * @property string $remark 备注
 * @property int $is_deleted 是否被删除：0否，1是
 * @property int $create_time 创建时间
 * @property int $update_time 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgConfig whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgConfig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgConfig whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgConfig whereItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgConfig whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgConfig whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgConfig whereValue($value)
 * @mixin \Eloquent
 */
class MsgConfig extends BaseModel
{

    protected $table = "msg_config";
    protected $primaryKey = "id";
    protected $keyType = "int";

}