<?php

namespace App\Models;

/**
 * App\Models\MsgPlatform
 *
 * @property int $id 主键ID
 * @property string $name 消息平台名称如：IT、EDU
 * @property int $status 状态：1正常，2禁用
 * @property int $is_deleted 是否被删除：0否，1是
 * @property int $create_time 创建时间
 * @property int $update_time 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgPlatform whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgPlatform whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgPlatform whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgPlatform whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgPlatform whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgPlatform whereUpdateTime($value)
 * @mixin \Eloquent
 */
class MsgPlatform extends BaseModel
{

    protected $table = "msg_platform";
    protected $primaryKey = "id";
    protected $keyType = "int";

}