<?php

namespace App\Models;

/**
 * App\Models\MsgChannel
 *
 * @property int $id 主键ID
 * @property string $name 消息渠道名称如：SMS、DINGTALK
 * @property int $status 状态：1正常，2禁用
 * @property int $is_deleted 是否被删除：0否，1是
 * @property int $create_time 创建时间
 * @property int $update_time 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgChannel whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgChannel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgChannel whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgChannel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgChannel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgChannel whereUpdateTime($value)
 * @mixin \Eloquent
 */
class MsgChannel extends BaseModel
{

    protected $table = "msg_channel";
    protected $primaryKey = "id";
    protected $keyType = "int";

}