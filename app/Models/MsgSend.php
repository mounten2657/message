<?php

namespace App\Models;

/**
 * App\Models\MsgSend
 *
 * @property int $id 主键ID
 * @property int $scene_id 消息场景来源ID，关联 nice_msg_scene.id
 * @property int $platform_id 消息平台ID，关联 nice_msg_platform.id
 * @property int $channel_id 消息渠道ID，关联 nice_msg_channel.id
 * @property string $task_id 消息推送任务ID
 * @property int $user_id 用户ID
 * @property string $mobile 手机号
 * @property string $realname 用户姓名
 * @property string $url 请求链接
 * @property string $method 请求方式：GET、POST
 * @property string $body 主体数据
 * @property string $result 请求结果
 * @property int $status 状态：1成功，2推送中，3失败
 * @property string $remark 备注
 * @property string $city 城市
 * @property string $campus 校区
 * @property string $role 角色
 * @property int $is_deleted 是否被删除：0否，1是
 * @property int $create_time 创建时间
 * @property int $update_time 更新时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereCampus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereChannelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereCreateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend wherePlatformId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereRealname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereSceneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MsgSend whereUserId($value)
 * @mixin \Eloquent
 */
class MsgSend extends BaseModel
{

    protected $table = "msg_send";
    protected $primaryKey = "id";
    protected $keyType = "int";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    

}
