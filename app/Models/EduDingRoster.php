<?php

namespace App\Models;

/**
 * App\Models\EduDingRoster
 *
 * @property int $id id
 * @property string|null $ding_userid 钉钉ID
 * @property string|null $unionid 钉钉唯一标识
 * @property string|null $name 姓名
 * @property string|null $tel 分机号
 * @property string|null $workplace 办公地点
 * @property string|null $remark 备注
 * @property string|null $mobile 手机号码
 * @property string|null $email 电子邮箱
 * @property string|null $orgemail 企业邮箱
 * @property int $active 是否激活
 * @property string|null $orderindepts 在对应的部门中的排序
 * @property int $isboss 是否为企业的老板
 * @property string|null $isleaderindepts
 * @property int $ishide
 * @property int $isadmin 是否为企业的管理员
 * @property string|null $department 成员所属部门id列表
 * @property string|null $position 职位信息
 * @property string|null $avatar
 * @property int $hireddate 入职时间
 * @property string|null $jobnumber 员工工号
 * @property string|null $extattr 扩展属性
 * @property int $issenior 是否是高管
 * @property string|null $statecode 国家地区码
 * @property int $status 状态
 * @property int $ctime 入库时间
 * @property int $cuid 添加人
 * @property int $mtime
 * @property int $muid
 * @property string|null $errcode 返回码
 * @property string|null $department_str 部门名称
 * @property int $city
 * @property int $campus
 * @property string|null $city_str
 * @property string|null $campus_str
 * @property int $leave_time 离职时间
 * @property string $political_status_str 政治面貌
 * @property string $vocational 所学专业
 * @property int $formal_time 转正日期
 * @property string $account_addr 家庭地址
 * @property string $account_type_str 户籍类型
 * @property int $gender 性别
 * @property string $employee_type_str 员工类型
 * @property string $probation 试用期
 * @property string $salary_card 银行卡号
 * @property string $id_card 身份证号
 * @property string $birthday 出生日期
 * @property string $school 毕业院校
 * @property string $emergency_contact 紧急联系人
 * @property string $emergency_phone 联系人电话
 * @property string $mailing_address 通信地址
 * @property string $marry_str 婚姻状况
 * @property int $graduate_time 毕业时间
 * @property string $employee_status_str 员工状态
 * @property string $clan 名族
 * @property string $education_str 学历
 * @property string $company 合同公司
 * @property int $first_contract_start 首次合同起始日
 * @property int $first_contract_end 首次合同到期日
 * @property int $now_contract_start 现合同起始日
 * @property int $now_contract_end 现合同到期日
 * @property string $graduate_date 毕业时间
 * @property string $gender_str 性别
 * @property string $hireddate_str
 * @property string $formal_time_str
 * @property int $role nice_admin  角色
 * @property string $nice_jobnumber 纳思工号
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereAccountAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereAccountTypeStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereCampus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereCampusStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereCityStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereClan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereCtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereCuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereDepartmentStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereDingUserid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereEducationStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereEmergencyContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereEmergencyPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereEmployeeStatusStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereEmployeeTypeStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereErrcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereExtattr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereFirstContractEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereFirstContractStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereFormalTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereFormalTimeStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereGenderStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereGraduateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereGraduateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereHireddate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereHireddateStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereIdCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereIsadmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereIsboss($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereIshide($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereIsleaderindepts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereIssenior($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereJobnumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereLeaveTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereMailingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereMarryStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereMtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereMuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereNiceJobnumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereNowContractEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereNowContractStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereOrderindepts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereOrgemail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster wherePoliticalStatusStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereProbation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereSalaryCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereSchool($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereStatecode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereUnionid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereVocational($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EduDingRoster whereWorkplace($value)
 * @mixin \Eloquent
 */
class EduDingRoster extends BaseModel
{

    protected $table = "ding_roster";
    protected $primaryKey = "id";
    protected $keyType = "int";
    public $connection = 'pgsql-edu';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    

}
