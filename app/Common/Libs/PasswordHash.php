<?php

namespace App\Common\Libs;

use Illuminate\Support\Facades\Hash;

/**
 * Security provides a set of methods to handle common security-related tasks.
 * In particular, Security supports the following features:
 * - Encryption/decryption: [[encryptByKey()]], [[decryptByKey()]], [[encryptByPassword()]] and [[decryptByPassword()]]
 * - Key derivation using standard algorithms: [[pbkdf2()]] and [[hkdf()]]
 * - Data tampering prevention: [[hashData()]] and [[validateData()]]
 * - Password validation: [[generatePasswordHash()]] and [[validatePassword()]]
 * > Note: this class requires 'OpenSSL' PHP extension for random key/string generation on Windows and
 * for encryption/decryption on all platforms. For the highest security level PHP version >= 5.5.0 is recommended.
 * Class PasswordHash
 * @package App\Common\Libs
 */
class PasswordHash
{

    /**
     * Generates a secure hash from a password and a random salt.
     * The generated hash can be stored in database.
     * Later when a password needs to be validated, the hash can be fetched and passed
     * @param string $password The password to be hashed.
     * @param array $options crypt options eg: ['rounds' => 12] | ['memory' => 1024,'time' => 2,'threads' => 2]
     * @return string The password hash string. When [[passwordHashStrategy]] is set to 'crypt',
     * the output is always 60 ASCII characters, when set to 'password_hash' the output length
     * might increase in future versions of PHP (https://secure.php.net/manual/en/function.password-hash.php)
     * @see validatePassword()
     */
    public static function generatePasswordHash($password, $options = [])
    {
        return Hash::make($password, $options);
    }

    /**
     * Verifies a password against a hash.
     * @param string $password The password to verify.
     * @param string $hash The hash to verify the password against.
     * @return bool whether the password is correct.
     * @see generatePasswordHash()
     */
    public static function validatePassword($password, $hash)
    {
        return Hash::check($password, $hash);
    }

}
