<?php

namespace App\Common\Libs;

use App\Consts\GlobalConst;

/**
 * 分页专用类
 * Class Pagination
 * @package App\Common\Libs
 */
class Pagination
{

    /** @var $page */
    public $page;

    /** @var $pageSize */
    public $pageSize;

    /**
     * get pagination
     * @param array $paginationParams
     * @return $this
     */
    public function getPagination($paginationParams = [])
    {
        $params = request()->all();
        if (!empty($paginationParams['not_use_pagination'])) {
            $this->page = 0;
            $this->pageSize = GlobalConst::BASE_PAGE_COUNT;
            return $this;
        }

        $this->page = !empty($params['page']) ? $params['page'] - 1 : 0;
        $this->page = $this->page < 0 ? 0 : $this->page;
        $this->pageSize = isset($params['page_size']) ? $params['page_size'] : GlobalConst::BASE_PAGE_COUNT;

        if (array_key_exists('page', $paginationParams)) {
            $this->page = $paginationParams['page'];
        }

        if (array_key_exists('page_size', $paginationParams)) {
            $this->pageSize = $paginationParams['page_size'];
        }

        $this->page = (int)$this->page;
        $this->pageSize = (int)$this->pageSize;
        return $this;
    }

}
