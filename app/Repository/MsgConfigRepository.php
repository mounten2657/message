<?php

namespace App\Repository;

use App\Consts\MessageConst;
use App\Models\MsgConfig;

/**
 * 消息配置仓库
 * Class MsgConfigRepository
 * @package App\Repository
 */
class MsgConfigRepository extends BaseRepository
{

    /**
     * MsgConfigRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new MsgConfig();
    }

    /**
     * get config by key
     * @param $key
     * @return string
     */
    public function getConfigByKey($key)
    {
        $info = $this->model
            ->where('item', $key)
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->orderByDesc('id')
            ->value('value');
        return $info ?: '';
    }

}