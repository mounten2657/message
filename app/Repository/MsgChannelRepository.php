<?php

namespace App\Repository;

use App\Consts\MessageConst;
use App\Models\MsgChannel;

/**
 * 消息渠道仓库
 * Class MsgChannelRepository
 * @package App\Repository
 */
class MsgChannelRepository extends BaseRepository
{

    /**
     * MsgChannelRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new MsgChannel();
    }

    /**
     * @return array
     */
    public function getList()
    {
        $list = $this->model
            ->where('status', MessageConst::STATUS_ENABLE)
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->select(['id', 'name'])
            ->get();
        return $list ? $list->toArray() : [];
    }

    /**
     * @param $id
     * @return string
     */
    public function getNameById($id)
    {
        $info = $this->model
            ->where('id', $id)
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->value('name');
        return $info ? : '';
    }

    /**
     * @param $name
     * @return string
     */
    public function getIdByName($name)
    {
        $info = $this->model
            ->where('name', $name)
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->value('id');
        return $info ? : '';
    }

}