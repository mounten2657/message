<?php

namespace App\Repository;

use App\Consts\MessageConst;
use App\Models\MsgPlatform;

/**
 * 消息平台仓库
 * Class MsgPlatformRepository
 * @package App\Repository
 */
class MsgPlatformRepository extends BaseRepository
{

    /**
     * MsgPlatformRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new MsgPlatform();
    }

    /**
     * @return array
     */
    public function getList()
    {
        $list = $this->model
            ->where('status', MessageConst::STATUS_ENABLE)
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->select(['id', 'name'])
            ->get();
        return $list ? $list->toArray() : [];
    }

    /**
     * @param $name
     * @return string
     */
    public function getIdByName($name)
    {
        $info = MsgPlatform::where('name', $name)
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->orderByDesc('id')
            ->value('id');
        return $info ? $info : '';
    }

    /**
     * @param $id
     * @return string
     */
    public function getNameById($id)
    {
        $info = MsgPlatform::where('id', $id)
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->orderByDesc('id')
            ->value('name');
        return $info ? $info : '';
    }

}