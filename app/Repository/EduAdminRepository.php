<?php

namespace App\Repository;

use App\Models\EduAdmin;

/**
 * EDU 用户信息仓库
 * Class EduAdminRepository
 * @package App\Repository
 */
class EduAdminRepository extends BaseRepository
{

    /**
     * EduAdminRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new EduAdmin();
    }

    /**
     * 根据手机号获取用户信息
     * @param $mobile
     * @return array
     */
    public function getInfoByMobile($mobile)
    {
        $info = $this->model
            ->where('username', $mobile)
            ->select(['userid as user_id', 'username as mobile', 'realname', 'role', 'city', 'campus'])
            ->orderByDesc('userid')
            ->first();
        return $info ? $info->toArray() : [];
    }

}