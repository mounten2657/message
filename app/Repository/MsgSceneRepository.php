<?php

namespace App\Repository;

use App\Consts\GlobalConst;
use App\Consts\MessageConst;
use App\Models\MsgScene;
use Illuminate\Database\Eloquent\Builder;

/**
 * 消息场景仓库
 * Class MsgSceneRepository
 * @package App\Repository
 */
class MsgSceneRepository extends BaseRepository
{

    /**
     * MsgSceneRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = MsgScene::getInstance();
    }

    /**
     * @return array
     */
    public function getList()
    {
        $list = $this->model
            ->where('status', MessageConst::STATUS_ENABLE)
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->select(['id', 'name'])
            ->get();
        return $list ? $list->toArray() : [];
    }

    /**
     * bound query
     * @param $where
     * @param array $field
     * @return Builder
     */
    public function boundQueryList($where, $field = ['*'])
    {
        return $this->model
            ->where(function (Builder $query) use($where) {
                if ($val = getVal($where, 'channel_id', [])) {
                    $val = is_array($val) ? $val : explode(',', $val);
                    $query->whereIn('channel_id', $val);
                }
                if ($val = getVal($where, 'platform_id', [])) {
                    $val = is_array($val) ? $val : explode(',', $val);
                    $query->whereIn('platform_id', $val);
                }
            })
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->select($field);
    }

    /**
     * 获取具体场景列表
     * @param $where
     * @param array $field
     * @return array
     */
    public function getDetailList($where, $field = ['*'])
    {
        $offset = getVal($where, 'offset', GlobalConst::DEFAULT_OFFSET);
        $limit = getVal($where, 'limit', GlobalConst::BASE_PAGE_COUNT);
        $list = $this->boundQueryList($where, $field)
            ->with(['platform', 'channel'])
            ->offset($offset)
            ->limit($limit)
            ->orderByDesc('id')
            ->get();
        return $list ? $list->toArray() : [];
    }

    /**
     * 获取具体列表总数
     * @param $where
     * @return int
     */
    public function getDetailListCount($where)
    {
        $count = $this->boundQueryList($where)
            ->count();
        return $count;
    }

    /**
     * 更新信息
     * @param $id
     * @param $data
     * @return int
     */
    public function updateInfoById($id, $data)
    {
        return $this->model
            ->where('id', $id)
            ->update($data);
    }

    /**
     * 获取场景值ID
     * @param $name
     * @param $platformId
     * @param $channelId
     * @return string
     */
    public function getIdByNameAndPlatformIdAndChannelId($name, $platformId, $channelId)
    {
        $info = $this->model->where('name', $name)
            ->where('platform_id', $platformId)
            ->where('channel_id', $channelId)
            ->where('status', MessageConst::STATUS_ENABLE)
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->orderByDesc('id')
            ->value('id');
        return $info ? $info : '';
    }

    /**
     * @param $id
     * @return string
     */
    public function getNameById($id)
    {
        $info = $this->model
            ->where('id', $id)
            ->where('status', MessageConst::STATUS_ENABLE)
            ->where('is_deleted', MessageConst::IS_DELETED_NO)
            ->value('name');
        return $info ? : '';
    }

}