<?php

namespace App\Repository;

/**
 * Class Repository
 * @return Repository
 * @package App\Repository
 * #Repository List:
 * @property MsgChannelRepository $MsgChannelRepository
 * @property MsgConfigRepository $MsgConfigRepository
 * @property MsgPlatformRepository $MsgPlatformRepository
 * @property MsgSceneRepository $MsgSceneRepository
 * @property MsgSendRepository $MsgSendRepository
 * @property EduAdminRepository $EduAdminRepository
 * @property EduDingRosterRepository $EduDingRosterRepository
 *
 * #Repository end.
 */
class Repository
{

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        // construction
    }

    /**
     * Handle dynamic static method calls into the model.
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }

    /**
     * Convert the model to its string representation.
     * @return string
     */
    public function __toString()
    {
        return json_encode($this);
    }

    /**
     * Get property in instance.
     * @param $property
     * @return mixed|null
     */
    public function __get($property)
    {
        if (isset($this->$property)) {
            return $this->$property;
        } else {
            $instance = 'App\\Repository\\' . $property;
            $service  = new $instance();
            return $this->$property = $service;
        }
    }

    /**
     * Set property for instance.
     * @param $property
     * @param $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    /**
     * Deny clone instance.
     * @return null
     */
    public function __clone()
    {
        return null;
    }

}
