<?php

namespace App\Repository;

use App\Models\EduDingRoster;

/**
 * EDU 钉钉用户花名册仓库
 * Class EduDingRosterRepository
 * @package App\Repository
 */
class EduDingRosterRepository extends BaseRepository
{

    /**
     * EduAdminRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new EduDingRoster();
    }

    /**
     * 获取用户手机号
     * @param $dingUserId
     * @return mixed|string
     */
    public function getMobileByDingUserId($dingUserId)
    {
        $info = $this->model
            ->where('ding_userid', $dingUserId)
            ->orderByDesc('id')
            ->value('mobile');
        return $info ? $info : '';
    }

}