<?php

namespace App\Services;

use App\Common\Libs\IdBuilder;
use App\Common\Libs\Json;
use App\Consts\ExceptionCodeConst;
use App\Consts\MessageConst;

/**
 * 推送服务类
 * Class PushService
 * @package App\Services
 */
class PushService extends BaseService
{

    /**
     * 获取新任务推送ID
     * @return string
     */
    public function getTaskId()
    {
        return IdBuilder::getUniqueID();
    }

    /**
     * 发送推送数据
     * @param $params
     * @param $taskId
     * @param $user
     * @param $body
     * @return bool|void
     */
    public function sendData($params, $taskId, $user, $body)
    {
        $remark = '';
        if (empty($user)) {
            // 流程不中断，给用户添加默认值，并更新备注
            $user = ['user_id' => 0, 'mobile' => $params['mobile'], 'realname' => '', 'role' => 0, 'city' => 0, 'campus' => 0];
            $remark = '查询不到用户信息，继续尝试推送';
        }
        $message = [
            'task_id' => $taskId,
            'channel_id' => $params['channel_id'],
            'platform_id' => $params['platform_id'],
            'scene_id' => $params['scene_id'],
            'user_id' => $user['user_id'],
            'mobile' => $user['mobile'],
            'realname' => $user['realname'],
            'role' => $user['role'],
            'city' => $user['city'],
            'campus' => $user['campus'],
            'status' => MessageConst::TASK_STATUS_NULL,
            'code' => $params['code'],
            'msg_type' => $params['msg_type'],
            'template' => $params['template'],
            'remark' => $remark,
            'body' => Json::encode($body),
        ];
        // 先入库，再推送
        $res = $this->MsgSendService->addMsgData($message);
        if (!$res) {
            return $this->throwError(lang('Save failed'), ExceptionCodeConst::SERVER_FAIL);
        }
        $messageId = $res;
        $body['message_id'] = $messageId;
        // 将数据推送到消息队列
        $res = $this->RabbitMQService->publisher($body);
        if (!$res) {
            return $this->throwError(lang( 'Push to rmq failed'), ExceptionCodeConst::CODE_FAIL);
        }
        // 更新推送状态
        $res = $this->MsgSendService->updateStatus($taskId, MessageConst::TASK_STATUS_PUSHING);
        if (!$res) {
            return $this->throwError(lang('Update task status failed'), ExceptionCodeConst::SERVER_FAIL);
        }

        return true;
    }

    /**
     * 接收推送数据
     * @param $messageId
     * @param $success
     * @param $res
     * @param $pushInfo
     * @return mixed
     */
    public function receiveData($messageId, $success, $res, $pushInfo)
    {
        // 结果判断，更新推送信息
        if ($success) {
            // 推送成功
            $status = MessageConst::TASK_STATUS_SUCCESS;
            $remark = lang('Push success');
        } else {
            // 推送失败
            $status = MessageConst::TASK_STATUS_FAILED;
            $remark = lang('Push failed');
        }
        $push = [
            'url' => $pushInfo['url'],
            'method' => $pushInfo['method'],
            'body' => json_encode($pushInfo['body']),
            'result' => json_encode($res),
            'status' => $status,
        ];
        $this->MsgSendService->updatePushInfo($messageId, $push, $remark);

        // 把第三方结果返回给 rmq
        return $res;
    }

}