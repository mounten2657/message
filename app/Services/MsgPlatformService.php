<?php

namespace App\Services;

use App\Consts\ExceptionCodeConst;
use App\Consts\MessageConst;

/**
 * 消息平台服务
 * Class PlatformService
 * @package App\Services
 */
class MsgPlatformService extends BaseService
{

    /**
     * MsgPlatformService constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->repository = repository()->MsgPlatformRepository;
    }

    /**
     * 获取平台列表
     * @return array
     */
    public function getPlatformList()
    {
        return $this->repository->getList();
    }

    /**
     * 获取平台ID
     * [!] 不存在则插入
     * @param $platform
     * @return mixed
     */
    public function getPlatformId($platform)
    {
        $info = $this->repository->getIdByName($platform);
        if (empty($info)) {
            $info = $this->repository->add([
                'name' => $platform,
                'status' => MessageConst::STATUS_ENABLE
            ]);
            if (intval($info) < 1) {
                return $this->throwError(lang('Save failed'), ExceptionCodeConst::SERVER_FAIL);
            }
        }
        return $info;
    }

    /**
     * 获取平台名
     * @param $id
     * @return string
     */
    public function getPlatformName($id)
    {
        return $this->repository->getNameById($id);
    }

}