<?php

namespace App\Services;

/**
 * EDU 用户信息服务
 * Class UserService
 * @package App\Services
 */
class UserService extends BaseService
{

    /**
     * UserService constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->repository = repository()->EduAdminRepository;
    }

    /**
     * 获取用户信息
     * @param $mobile
     * @return array
     */
    public function getUser($mobile)
    {
        return $this->repository->getInfoByMobile($mobile);
    }

    /**
     * 获取用户信息 - 通过钉钉用户ID
     * @param $dingUserId
     * @return array
     */
    public function getUserByDingUserId($dingUserId)
    {
        $mobile = repository()->EduDingRosterRepository->getMobileByDingUserId($dingUserId);
        if ($mobile) {
            return $this->getUser($mobile);
        }
        return [];
    }

}