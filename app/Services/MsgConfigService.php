<?php

namespace App\Services;

/**
 * 消息配置服务
 * Class ConfigService
 * @package App\Services
 */
class MsgConfigService extends BaseService
{

    /**
     * MsgConfigService constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->repository = repository()->MsgConfigRepository;
    }

    /**
     * get config by key
     * @param $key
     * @return bool
     */
    public function getConfig($key)
    {
        return getConfigByKey($key);
    }

}