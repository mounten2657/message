<?php

namespace App\Services;

/**
 * 消息渠道服务
 * Class ChannelService
 * @package App\Services
 */
class MsgChannelService extends BaseService
{

    /**
     * MsgChannelService constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->repository = repository()->MsgChannelRepository;
    }

    /**
     * 获取渠道列表
     * @return array
     */
    public function getChannelList()
    {
        return $this->repository->getList();
    }

    /**
     * 获取渠道名
     * @param $id
     * @return string
     */
    public function getChanelName($id)
    {
        return $this->repository->getNameById($id);
    }

}