<?php

namespace App\Services;

use App\Consts\ExceptionCodeConst;
use App\Consts\MessageConst;

/**
 * 统一发送服务
 * Class UnionService
 * @package App\Services
 */
class UnionService extends MsgSendService
{

    /**
     * 统一发送消息
     * @param $params
     * @return array|mixed
     */
    public function sendMsg($params)
    {
        $key = getVal($params, 'scene_key');
        $userList = getVal($params, 'user_list');
        $contentJson = getVal($params, 'content_json');
        if (empty($key) || empty($userList) || empty($contentJson)) {
            return $this->error();
        }
        $scene = $this->checkKey($key);
        if (isset($scene['code'])) {
            return $scene;
        }
        if ($scene['channel_id'] == MessageConst::CHANNEL_SMS_ID) {
            // 短信
            $body = [
                'channel_id' => $scene['channel_id'],
                'mobile' => $userList,
                'project_id' => $scene['msg_type_template'],
                'sms_type' => $scene['msg_type_id'],
                'var_json' => $contentJson,
                'platform' => $scene['platform_name'],
                'scene' => $scene['name'],
                'scene_id' => $scene['id'],
            ];
            $res = $this->SmsService->sendSmsMsg($body);
            if (isset($res['code'])) {
                return $res;
            }
        } elseif ($scene['channel_id'] == MessageConst::CHANNEL_DINGTALK_ID) {
            // 钉钉
            $body = [
                'channel_id' => $scene['channel_id'],
                "msg" => $contentJson,
                "msg_type" => $scene['msg_type_id'],
                'platform' => $scene['platform_name'],
                'scene' => $scene['name'],
                'scene_id' => $scene['id'],
            ];
            $isMobile = false;
            $mobileList = explode(',', $userList);
            if (isset($mobileList[0]) && $mobileList[0]) {
                $isMobile = preg_match("/^1[23456789]\d{9}$/", $mobileList[0]);
            }
            if ($isMobile) {
                $body['mobile_list'] = $userList;
            } else {
                $body['userid_list'] = $userList;
            }
            $res = $this->DingService->sendDingMsg($body);
            if (isset($res['code'])) {
                return $res;
            }
        } else {
            return $this->error(lang('Scene Channel Invalid'), ExceptionCodeConst::SCENE_CHANNEL_INVALID, $params);
        }
        // 返回结果
        return $res;
    }

    /**
     * check scene key
     * @param $key
     * @return array|mixed
     */
    protected function checkKey($key)
    {
        $scene = $this->MsgSceneService->getSceneInfo([['key', 'like', "%$key%"]]);
        if (isset($scene['code'])) {
            return $this->error(lang('Key Invalid'), ExceptionCodeConst::SCENE_KEY_INVALID);
        }
        if ($scene['key'] !== $key) {
            return $this->error(lang('Lower Version'), ExceptionCodeConst::SCENE_KEY_VERSION);
        }
        if ($scene['status'] == MessageConst::STATUS_DISABLE) {
            return $this->error(lang('Scene Closed'), ExceptionCodeConst::SCENE_STATUS_OFF);
        }
        // @togo check platform
        return $scene;
    }

}