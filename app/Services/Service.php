<?php

namespace App\Services;

/**
 * Class Service
 * @return Service
 * @package App\Services
 * #Service List:
 * @property RabbitMQService $RabbitMQService
 * @property DingService $DingService
 * @property SmsService $SmsService
 * @property MsgChannelService $MsgChannelService
 * @property MsgSceneService $MsgSceneService
 * @property MsgPlatformService $MsgPlatformService
 * @property MsgConfigService $MsgConfigService
 * @property MsgSendService $MsgSendService
 * @property UserService $UserService
 * @property PushService $PushService
 * @property UnionService $UnionService
 *
 * #Service end.
 */
class Service
{

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        // construction
    }

    /**
     * Handle dynamic static method calls into the model.
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }

    /**
     * Convert the model to its string representation.
     * @return string
     */
    public function __toString()
    {
        return json_encode($this);
    }

    /**
     * Get property in instance.
     * @param $property
     * @return mixed|null
     */
    public function __get($property)
    {
        if (isset($this->$property)) {
            return $this->$property;
        } else {
            $instance = 'App\\Services\\' . $property;
            $service  = new $instance();
            return $this->$property = $service;
        }
    }

    /**
     * Set property for instance.
     * @param $property
     * @param $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    /**
     * Deny clone instance.
     * @return null
     */
    public function __clone()
    {
        return null;
    }

}
