<?php

namespace App\Services;

use App\Consts\GlobalConst;
use App\Consts\SmsConst;

/**
 * 消息配置服务
 * Class SmsService
 * @package App\Services
 */
class SmsService extends BaseService
{

    /**
     * 生成验证码
     * @return int
     */
    public function generateCode()
    {
        return rand(1000, 9999);
    }

    /**
     * 入队列 - 发送短信消息
     * @param $params
     * @return mixed
     */
    public function sendSmsMsg($params)
    {
        $taskId = $this->PushService->getTaskId();
        $channelId = $params['channel_id'];
        $platform = $params['platform'];
        $scene = $params['scene'];
        $mobileList = $params['mobile'];
        $mobileList = explode(',', $mobileList);
        // 填充默认值
        $params['code'] = !empty($params['code']) ? $params['code'] : '';
        $params['template'] = !empty($params['project_id']) ? $params['project_id'] : SmsConst::SMS_PROJECT_CODE_NICE;
        $params['var_json'] = !empty($params['var_json']) ? $params['var_json'] : '';
        $params['msg_type'] = !empty($params['msg_type']) ? $params['msg_type'] : SmsConst::SMS_MSG_TYPE_CODE;
        if (SmsConst::SMS_MSG_TYPE_CODE == $params['msg_type']) {
            if (empty($params['var_json'])) {
                $params['code'] = $params['code'] ? : $this->generateCode();
                $params['var_json'] = json_encode(['code' => $params['code']]);
            } else {
                $varJson = json_decode($params['var_json'], true);
                $params['code'] = !empty($varJson['code']) ? $varJson['code'] : $params['code'];
            }
        }

        // 获取场景值
        $sceneInfo= $this->MsgSceneService->getScene($scene, $platform, $channelId);
        $params['platform_id']= $sceneInfo['platform_id'];
        $params['scene_id'] = $sceneInfo['scene_id'];

        foreach ($mobileList as $mobile) {
            // 获取推送信息
            $body = $this->RabbitMQService->getFormatBody(GlobalConst::RMQ_QUEUE_TYPE_MSG,
                'sendSmsMsgCallback',
                [
                    'mobile' => $mobile,
                    'project_id' => $params['template'],
                    'sms_type' => $params['msg_type'],
                    'var_json' => $params['var_json'],
                ],
                $taskId
            );

            // 获取用户信息
            $user = $this->UserService->getUser($mobile);

            // 发送推送数据
            $this->PushService->sendData($params, $taskId, $user, $body);
        }

        // 返回任务ID
        $res = ['task_id' => $taskId];
        if (SmsConst::SMS_MSG_TYPE_CODE == $params['msg_type']) {
            $res['verify_code'] = $params['code'];
        }
        return $res;
    }

    /**
     * 回调 - 发送短信通知
     * @param $body
     * @return mixed
     */
    public function sendSmsMsgCallback($body)
    {
        $data = $body['data'];
        $messageId = !empty($body['message_id']) ? $body['message_id'] : 0;
        $this->validate($data, [
            'mobile' => 'required|phone',
            'project_id' => 'required|string|max:100',
            'sms_type' => 'required|max:10',
            'var_json' => 'required|json|max:10000',
        ]);
        $mobile = $data['mobile'];
        $projectId = $data['project_id'];
        $varJson = json_decode($data['var_json'], true);
        $res = sendSubMailMsg($mobile, $projectId, $varJson);

        $smsInfo = SmsConst::SMS_TEMPLATE_LIST;
        $pushInfo = [
            'url' => !empty($smsInfo[$projectId]['url']) ? $smsInfo[$projectId]['url'] : '',
            'method' => !empty($smsInfo[$projectId]['method']) ? $smsInfo[$projectId]['method'] : '',
            'body' => !empty($smsInfo[$projectId]['content']) ? vsprintf($smsInfo[$projectId]['content'], array_values($varJson)) : $data,
        ];
        $success = !empty($res['status']) && 'success' == $res['status'];

        // 返回数据到服务
        return $this->PushService->receiveData($messageId, $success, $res, $pushInfo);
    }

    /**
     * 验证短信验证码是否正确
     * @param $taskId
     * @param $mobile
     * @param $code
     * @return bool
     */
    public function verifyCode($taskId, $mobile, $code)
    {
        $verify = $this->MsgSendService->getCode($taskId, $mobile);
        return $verify == $code;
    }

}