<?php


namespace App\Consts;

/**
 * 钉钉相关常量
 * Class DingTalkConst
 * @package App\Consts
 */
class DingTalkConst
{

    // 钉钉消息通知类型
    const DINGTALK_MSG_NOTICE_TYPE_WORK = 100;

    // 钉钉消息类型
    const DINGTALK_MSG_TYPE_BUSINESS_DAILY = 1;                                    // 经营日报
    const DINGTALK_MSG_TYPE_TEACHER_ELIMINATION_REMINDER = 11;                     // 教师消课提醒
    const DINGTALK_MSG_TYPE_PRINCIPAL_TODAY_BRIEFING = 12;                         // 校长今日简报
    const DINGTALK_MSG_TYPE_PRODUCT_USE_BRIEFING_YESTERDAY = 13;                   // 产品昨日使用简报
    const DINGTALK_MSG_TYPE_LEADERSHIP_FLOW_RANKING = 14;                          // 领导-流水排行
    const DINGTALK_MSG_TYPE_LEADERSHIP_INDICATORS_COMPLETED = 15;                  // 领导-指标完成
    const DINGTALK_MSG_TYPE_SYSTEM_ANNOUNCEMENT = 16;                              // 系统公告
    const DINGTALK_MSG_TYPE_RECRUITMENT_DATA_REMINDER = 17;                        // 招聘数据提醒
    const DINGTALK_MSG_TYPE_REMINDER_BEFORE_CLASS = 18;                            // 学管师课前提醒
    const DINGTALK_MSG_TYPE_TEACHER_REMINDER_OF_TOMORROWS_CLASS_SCHEDULE = 19;     // 教师明日课表提醒
    const DINGTALK_MSG_TYPE_PARENT_FEEDBACK_DAILY_PRINCIPAL = 20;                  // 家长反馈日报-校长
    const DINGTALK_MSG_TYPE_PARENT_FEEDBACK_DAILY_BRANCH = 21;                     // 家长反馈日报-分公司
    const DINGTALK_MSG_TYPE_PARENT_FEEDBACK_WEEKLY_CAMPUS = 22;                    // 家长反馈周报-校区
    const DINGTALK_MSG_TYPE_PARENT_FEEDBACK_WEEKLY_BRANCH = 23;                    // 家长反馈周报-分公司
    const DINGTALK_MSG_TYPE_NOTICE_OF_EXPIRED_COURSES = 24;                        // 失效课程通知
    const DINGTALK_MSG_TYPE_NOTICE_OF_CONFIRMATION_OF_DEDUCTION_AND_PENALTY = 25;  // 提点扣罚确认通知
    const DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_CONSULTATION_INDIVIDUAL = 26;   // 诺约通知-咨询个人
    const DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_PRINCIPAL_CONSULTER = 27;       // 诺约通知-校长-主管
    const DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_REGIONAL_MANAGER = 28;          // 诺约通知-区域经理
    const DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_BRANCH_MANAGER = 29;            // 诺约通知-分公司经理
    const DINGTALK_MSG_TYPE_STUDENTS_PAY_ATTENTION_TO_THE_RANKING = 30;            // 学员关注排行
    const DINGTALK_MSG_TYPE_ADMINISTRATIVE_APPOINTMENT_NOTICE = 31;                // 行政预约通知
    const DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE = 32;                                 // IT研发工单通知-处理中
    const DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE_TEST = 33;                            // IT研发工单通知-已提测
    const DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE_CHECK = 34;                           // IT研发工单通知-待验收

    // 钉钉消息模板类型
    const DINGTALK_TEMPLATE_LIST = [
        self::DINGTALK_MSG_TYPE_BUSINESS_DAILY => [
            'id' => self::DINGTALK_MSG_TYPE_BUSINESS_DAILY,
            'name' => '经营日报',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_BUSINESS_DAILY,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_TEACHER_ELIMINATION_REMINDER => [
            'id' => self::DINGTALK_MSG_TYPE_TEACHER_ELIMINATION_REMINDER,
            'name' => '教师消课提醒',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_TEACHER_ELIMINATION_REMINDER,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_PRINCIPAL_TODAY_BRIEFING => [
            'id' => self::DINGTALK_MSG_TYPE_PRINCIPAL_TODAY_BRIEFING,
            'name' => '校长今日简报',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_PRINCIPAL_TODAY_BRIEFING,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_PRODUCT_USE_BRIEFING_YESTERDAY => [
            'id' => self::DINGTALK_MSG_TYPE_PRODUCT_USE_BRIEFING_YESTERDAY,
            'name' => '产品昨日使用简报',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_PRODUCT_USE_BRIEFING_YESTERDAY,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_LEADERSHIP_FLOW_RANKING => [
            'id' => self::DINGTALK_MSG_TYPE_LEADERSHIP_FLOW_RANKING,
            'name' => '领导-流水排行',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_LEADERSHIP_FLOW_RANKING,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_LEADERSHIP_INDICATORS_COMPLETED => [
            'id' => self::DINGTALK_MSG_TYPE_LEADERSHIP_INDICATORS_COMPLETED,
            'name' => '领导-指标完成',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_LEADERSHIP_INDICATORS_COMPLETED,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_SYSTEM_ANNOUNCEMENT => [
            'id' => self::DINGTALK_MSG_TYPE_SYSTEM_ANNOUNCEMENT,
            'name' => '系统公告',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_SYSTEM_ANNOUNCEMENT,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_RECRUITMENT_DATA_REMINDER => [
            'id' => self::DINGTALK_MSG_TYPE_RECRUITMENT_DATA_REMINDER,
            'name' => '招聘数据提醒',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_RECRUITMENT_DATA_REMINDER,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_REMINDER_BEFORE_CLASS => [
            'id' => self::DINGTALK_MSG_TYPE_REMINDER_BEFORE_CLASS,
            'name' => '学管师课前提醒',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_REMINDER_BEFORE_CLASS,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_TEACHER_REMINDER_OF_TOMORROWS_CLASS_SCHEDULE => [
            'id' => self::DINGTALK_MSG_TYPE_TEACHER_REMINDER_OF_TOMORROWS_CLASS_SCHEDULE,
            'name' => '教师明日课表提醒',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_TEACHER_REMINDER_OF_TOMORROWS_CLASS_SCHEDULE,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_DAILY_PRINCIPAL => [
            'id' => self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_DAILY_PRINCIPAL,
            'name' => '家长反馈日报-校长',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_DAILY_PRINCIPAL,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_DAILY_BRANCH => [
            'id' => self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_DAILY_BRANCH,
            'name' => '家长反馈日报-分公司',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_DAILY_BRANCH,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_WEEKLY_CAMPUS => [
            'id' => self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_WEEKLY_CAMPUS,
            'name' => '家长反馈周报-校区',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_WEEKLY_CAMPUS,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_WEEKLY_BRANCH => [
            'id' => self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_WEEKLY_BRANCH,
            'name' => '家长反馈周报-分公司',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_PARENT_FEEDBACK_WEEKLY_BRANCH,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_NOTICE_OF_EXPIRED_COURSES => [
            'id' => self::DINGTALK_MSG_TYPE_NOTICE_OF_EXPIRED_COURSES,
            'name' => '失效课程通知',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_NOTICE_OF_EXPIRED_COURSES,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_NOTICE_OF_CONFIRMATION_OF_DEDUCTION_AND_PENALTY => [
            'id' => self::DINGTALK_MSG_TYPE_NOTICE_OF_CONFIRMATION_OF_DEDUCTION_AND_PENALTY,
            'name' => '提点扣罚确认通知',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_NOTICE_OF_CONFIRMATION_OF_DEDUCTION_AND_PENALTY,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_CONSULTATION_INDIVIDUAL => [
            'id' => self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_CONSULTATION_INDIVIDUAL,
            'name' => '诺约通知-咨询个人',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_CONSULTATION_INDIVIDUAL,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_PRINCIPAL_CONSULTER => [
            'id' => self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_PRINCIPAL_CONSULTER,
            'name' => '诺约通知-校长-主管',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_PRINCIPAL_CONSULTER,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_REGIONAL_MANAGER => [
            'id' => self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_REGIONAL_MANAGER,
            'name' => '诺约通知-区域经理',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_REGIONAL_MANAGER,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_BRANCH_MANAGER => [
            'id' => self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_BRANCH_MANAGER,
            'name' => '诺约通知-分公司经理',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_NOYO_STATISTICS_NOTICE_BRANCH_MANAGER,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_STUDENTS_PAY_ATTENTION_TO_THE_RANKING => [
            'id' => self::DINGTALK_MSG_TYPE_STUDENTS_PAY_ATTENTION_TO_THE_RANKING,
            'name' => '学员关注排行',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_STUDENTS_PAY_ATTENTION_TO_THE_RANKING,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_ADMINISTRATIVE_APPOINTMENT_NOTICE => [
            'id' => self::DINGTALK_MSG_TYPE_ADMINISTRATIVE_APPOINTMENT_NOTICE,
            'name' => '行政预约通知',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_ADMINISTRATIVE_APPOINTMENT_NOTICE,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '%s',
                    'text' => "%s",
                ],
            ],
            'common_user' => [],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE => [
            'id' => self::DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE,
            'name' => 'IT研发工单通知-处理中',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '您有任务待处理',
                    'text' => "## 您有任务待处理： \n - 任务名： **%s** \n - 处理人： **%s** \n - 开始： %s\n - 结束： %s\n - 状态： %s\n - 详情： [点击查看](%s)",
                ],
            ],
            'common_user' => [
                //['mobile' => '13735534960', 'ding_user_id' => '061204434720604551', 'real_name' => '付长华'],
                ['mobile' => '13516720310', 'ding_user_id' => '15989451606397831', 'real_name' => '陆朝斌'],
                //['mobile' => '15757117946', 'ding_user_id' => '1620265435874368', 'real_name' => '吴军'],
            ],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE_TEST => [
            'id' => self::DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE_TEST,
            'name' => 'IT研发工单通知-已提测',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE_TEST,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '您有任务待测试',
                    'text' => "## 您有任务待测试： \n - 任务名： **%s** \n - 测试人： **%s** \n - 开始： %s\n - 结束： %s\n - 状态： %s\n - 详情： [点击查看](%s)",
                ],
            ],
            'common_user' => [
                //['mobile' => '13735534960', 'ding_user_id' => '061204434720604551', 'real_name' => '付长华'],
                ['mobile' => '13516720310', 'ding_user_id' => '15989451606397831', 'real_name' => '陆朝斌'],
                ['mobile' => '15267156332', 'ding_user_id' => '075343036323237202', 'real_name' => '孟小波']
                //['mobile' => '15757117946', 'ding_user_id' => '1620265435874368', 'real_name' => '吴军'],
            ],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
        self::DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE_CHECK => [
            'id' => self::DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE_CHECK,
            'name' => 'IT研发工单通知-待验收',
            'template' => self::DINGTALK_MSG_NOTICE_TYPE_WORK,
            'msg_type' => self::DINGTALK_MSG_TYPE_IT_DEMAND_NOTICE_CHECK,
            'content' => [
                'msgtype' => 'markdown',
                'markdown' => [
                    'title' => '您有任务待验收',
                    'text' => "## 您有任务待验收： \n - 任务名： **%s** \n - 验收人： **%s** \n - 开始： %s\n - 结束： %s\n - 状态： %s\n - 详情： [点击查看](%s)",
                ],
            ],
            'common_user' => [
                //['mobile' => '13735534960', 'ding_user_id' => '061204434720604551', 'real_name' => '付长华'],
                ['mobile' => '13516720310', 'ding_user_id' => '15989451606397831', 'real_name' => '陆朝斌'],
                //['mobile' => '15757117946', 'ding_user_id' => '1620265435874368', 'real_name' => '吴军']
            ],
            'url' => 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2',
            'method' => 'POST',
            'callback' => 'send',
        ],
    ];


}