<?php

namespace App\Consts;

/**
 * 消息推送相关常量
 * Class MessageConst
 * @package App\Consts
 */
class MessageConst
{

    // 消息统计基数，如果追求真实效果可置为 0
    const STAT_BASE_COUNT = 0;

    // 启用状态
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    // 推送状态
    const TASK_STATUS_NULL = 0;
    const TASK_STATUS_PUSHING = 1;
    const TASK_STATUS_SUCCESS = 2;
    const TASK_STATUS_FAILED = 3;

    // 推送状态列表
    const TASK_STATUS_LIST = [
        self::TASK_STATUS_NULL => [
            'id' => self::TASK_STATUS_NULL,
            'name' => '未推送',
        ],
        self::TASK_STATUS_PUSHING => [
            'id' => self::TASK_STATUS_PUSHING,
            'name' => '推送中',
        ],
        self::TASK_STATUS_SUCCESS => [
            'id' => self::TASK_STATUS_SUCCESS,
            'name' => '推送成功',
        ],
        self::TASK_STATUS_FAILED => [
            'id' => self::TASK_STATUS_FAILED,
            'name' => '推送失败',
        ],
    ];

    // 删除状态
    const IS_DELETED_NO = 0;
    const IS_DELETED_YES = 0;

    // 消息渠道
    const CHANNEL_SMS = 'SMS';
    const CHANNEL_SMS_ID = 1;
    const CHANNEL_SMS_NAME = '短信推送';
    const CHANNEL_SMS_SN = '短信';
    const CHANNEL_DINGTALK = 'DINGTALK';
    const CHANNEL_DINGTALK_ID = 2;
    const CHANNEL_DINGTALK_NAME = '钉钉推送';
    const CHANNEL_DINGTALK_SN = '钉钉';
    const CHANNEL_OTHER = 'OTHER';
    const CHANNEL_OTHER_ID = 999;
    const CHANNEL_OTHER_NAME = '其他推送';
    const CHANNEL_OTHER_SN = '其他';

    // 消息平台
    const PLATFORM_MADMIN = 'MADMIN';
    const PLATFORM_IT = 'IT';
    const PLATFORM_API = 'API';
    const PLATFORM_PAY = 'PAY';

}