<?php


namespace App\Consts;

/**
 * 缓存键名定义
 * Class CacheKeyConst
 * @package App\Consts
 */
class CacheKeyConst
{

    /**
     * @var string 钉钉 access token
     * %s : app_key
     */
    const DINGTALK_ACCESS_TOKEN = 'dingtalk_access_token_%s';

    /**
     * @var string submail config
     */
    const SUBMAIL_CONFIGURE = 'submail_configure';

    /** @var string blocking rabbit mq status */
    const BLOCKING_RABBIT_MQ_STATUS = 'blocking_rabbit_mq_status';

    /** @var string blocking schedule status */
    const BLOCKING_SCHEDULE_STATUS = 'blocking_schedule_status';

}