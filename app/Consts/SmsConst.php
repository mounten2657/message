<?php

namespace App\Consts;

/**
 * 短信相关常量
 * Class SmsConst
 * @package App\Consts
 */
class SmsConst
{

    // 验证码过期时间
    const SMS_CODE_EXPIRED = 600;                  // 十分钟

    // 短信消息类型
    const SMS_MSG_TYPE_CODE = 1;                   // 验证码
    const SMS_MSG_TYPE_REPORT = 2;                 // 课堂报告
    const SMS_MSG_TYPE_PAY = 3;                    // 缴费通知
    const SMS_MSG_TYPE_ORDER = 4;                  // 订单通知
    const SMS_MSG_TYPE_PRINCIPAL = 5;              // 校长日报
    const SMS_MSG_TYPE_TEACHER = 6;                // 老师消课提醒

    // 短信项目类型
    const SMS_PROJECT_CODE_NICE = 'zp1tL1';        // 纳思书院验证码
    const SMS_PROJECT_CODE_NITE = '0uZ8b1';        // 纳思在线验证码
    const SMS_PROJECT_REPORT = '7d6ea4';           // 纳思书院课堂报告
    const SMS_PROJECT_PAY = 'CbZH2';               // 纳思书院缴费
    const SMS_PROJECT_ORDER = '9pbXz';             // 纳思书院订单
    const SMS_PROJECT_PRINCIPAL_SELF = 'vWZJX1';   // 纳思书院校长日报-校长
    const SMS_PROJECT_PRINCIPAL_CEO = 'WTuzV3';    // 纳思书院校长日报-管理者
    const SMS_PROJECT_TEACHER = 'T0Vcy3';          // 纳思书院老师消课提醒

    // 短信项目类型映射
    const SMS_TYPE_CODE_NICE = 201;         // 纳思书院验证码
    const SMS_TYPE_CODE_NITE = 202;         // 纳思在线验证码
    const SMS_TYPE_REPORT = 203;            // 纳思书院课堂报告
    const SMS_TYPE_PAY = 204;               // 纳思书院缴费
    const SMS_TYPE_ORDER = 205;             // 纳思书院订单
    const SMS_TYPE_PRINCIPAL_SELF = 207;    // 纳思书院校长日报-校长
    const SMS_TYPE_PRINCIPAL_CEO = 208;     // 纳思书院校长日报-管理者
    const SMS_TYPE_TEACHER = 209;           // 纳思书院老师消课提醒

    // 短信模板列表
    const SMS_TEMPLATE_LIST = [
        self::SMS_PROJECT_CODE_NICE => [
            'id' => self::SMS_TYPE_CODE_NICE,
            'name' => '纳思书院验证码',
            'template' => self::SMS_PROJECT_CODE_NICE,
            'msg_type' => self::SMS_MSG_TYPE_CODE,
            'content' => "【纳思书院】您的验证码：%s，十分钟内有效，如非本人操作请忽略。",
            'url' => 'http://api.submail.cn/message/xsend.json',
            'method' => 'POST',
            'callback' => 'sendcode',
        ],
        self::SMS_PROJECT_CODE_NITE => [
            'id' => self::SMS_TYPE_CODE_NITE,
            'name' => '纳思在线验证码',
            'template' => self::SMS_PROJECT_CODE_NITE,
            'msg_type' => self::SMS_MSG_TYPE_CODE,
            'content' => "【纳思在线】您的验证码：%s，十分钟内有效，如非本人操作请忽略。",
            'url' => 'http://api.submail.cn/message/xsend.json',
            'method' => 'POST',
            'callback' => 'sendcode',
        ],
        self::SMS_PROJECT_REPORT => [
            'id' => self::SMS_TYPE_REPORT,
            'name' => '课堂报告',
            'template' => self::SMS_PROJECT_REPORT,
            'msg_type' => self::SMS_MSG_TYPE_REPORT,
            'content' => "【纳思书院】您好，"
                .'%s'."同学在纳思书院"
                .'%s'."学习中心完成一次课堂学习，科目："
                .'%s'."，上课时间："
                .'%s'."，授课老师："
                .'%s'."，详情查看："
                .'%s'."，如有疑问，请联系您的授课老师、学管老师。",
            'url' => 'http://api.submail.cn/message/xsend.json',
            'method' => 'POST',
            'callback' => 'sendEndOfClassNotice',
        ],
        self::SMS_PROJECT_PAY => [
            'id' => self::SMS_TYPE_PAY,
            'name' => '缴费通知',
            'template' => self::SMS_PROJECT_PAY,
            'msg_type' => self::SMS_MSG_TYPE_PAY,
            'content' => "【纳思书院】恭喜您，缴费成功，%s",
            'url' => 'http://api.submail.cn/message/xsend.json',
            'method' => 'POST',
            'callback' => 'sendFeeIN',
        ],
        self::SMS_PROJECT_ORDER => [
            'id' => self::SMS_TYPE_ORDER,
            'name' => '订单通知',
            'template' => self::SMS_PROJECT_ORDER,
            'msg_type' => self::SMS_MSG_TYPE_ORDER,
            'content' => "【纳思书院】您有一个新的订单，%s",
            'url' => 'http://api.submail.cn/message/xsend.json',
            'method' => 'POST',
            'callback' => 'sendClassIn',
        ],
        self::SMS_PROJECT_PRINCIPAL_SELF => [
            'id' => self::SMS_TYPE_PRINCIPAL_SELF,
            'name' => '校长日报-校长',
            'template' => self::SMS_PROJECT_PRINCIPAL_SELF,
            'msg_type' => self::SMS_MSG_TYPE_PRINCIPAL,
            'content' => "【纳思书院】"
                .'%s'."您好，"
                .'%s'."校区"
                .'%s'."经营日报，具体数据如下："
                .'%s',
            'url' => 'http://api.submail.cn/message/xsend.json',
            'method' => 'POST',
            'callback' => 'sendDayDataForXZ',
        ],
        self::SMS_PROJECT_PRINCIPAL_CEO => [
            'id' => self::SMS_TYPE_PRINCIPAL_CEO,
            'name' => '校长日报-管理者',
            'template' => self::SMS_PROJECT_PRINCIPAL_CEO,
            'msg_type' => self::SMS_MSG_TYPE_PRINCIPAL,
            'content' => "【纳思书院】"
                .'%s'."您好，"
                .'%s'."经营日报，具体如下："
                .'%s',
            'url' => 'http://api.submail.cn/message/xsend.json',
            'method' => 'POST',
            'callback' => 'sendDayDataForCEO',
        ],
        self::SMS_PROJECT_TEACHER => [
            'id' => self::SMS_TYPE_TEACHER,
            'name' => '老师消课提醒',
            'template' => self::SMS_PROJECT_TEACHER,
            'msg_type' => self::SMS_MSG_TYPE_TEACHER,
            'content' => "【纳思书院】"
                .'%s'."老师，晚上好！智能小助手检测到今天还有"
                .'%s'."次课未消，友情提醒您及时处理喔！工作一天辛苦啦，纳思有您更精彩！",
            'url' => 'http://api.submail.cn/message/xsend.json',
            'method' => 'POST',
            'callback' => 'callAttantionToTeacher',
        ],
    ];

}