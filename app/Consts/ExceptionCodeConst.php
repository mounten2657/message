<?php

namespace App\Consts;

/**
 * 错误异常码常量定义
 * Class ErrorCodeConst
 * @package App\Consts
 */
class ExceptionCodeConst
{

    // 公用
    const OPERATING_SUCCESS = 200;         // 操作成功
    const ROUTE_NOT_FOUND = 404;           // 路由未找到
    const ROUTE_NOT_ALLOWED = 405;         // 请求方式不允许
    const ROUTE_PAGE_EXPIRED = 419;        // 页面过期
    const CODE_FAIL = 500;                 // 代码错误
    const SERVER_FAIL = 599;               // 服务错误
    const MISSING_PARAMETER = 998;         // 缺少必要参数
    const ILLEGAL_PARAMETER = 999;         // 参数错误
    const API_SUCCESS = 2000;              // 接口返回成功

    // 签名
    const SIGN_PLATFORM_ERROR = 230100;  // 签名解析后平台错误
    const SIGN_VERIFY_FAIL = 230101;     // 签名验证失败
    const SIGN_TIME_OVER = 230103;       // 传入时间戳大于服务器时间 + N秒
    const PAGE_TIME_OVER = 230104;       // 页面已过期

    // 场景相关
    const SCENE_KEY_INVALID = 240101;
    const SCENE_KEY_VERSION = 240102;
    const SCENE_STATUS_OFF = 240103;
    const SCENE_CHANNEL_INVALID = 240104;


}