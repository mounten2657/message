<?php


namespace App\Consts;

/**
 * 消息推送配置键名
 * Class MessageConfigConst
 * @package App\Consts
 */
class MessageConfigConst
{

    // 平台密钥
    const MSG_CONFIG_KEY_SECRET = 'SECRET_MSG_CENTER';
    // 页面密钥
    const MSG_CONFIG_KEY_PAGE_SECRET = 'SECRET_MSG_CENTER_PAGES';

    // 短信类型
    const MSG_CONFIG_KEY_SMS_TYPE = 'MSG_TYPE_SMS';
    // 钉钉类型
    const MSG_CONFIG_KEY_DINGTALK_TYPE = 'MSG_TYPE_DINGTALK';

}