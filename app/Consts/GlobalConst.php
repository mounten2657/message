<?php

namespace App\Consts;

/**
 * 全局通用常量定义
 * Class GlobalConst
 * @package App\Consts
 */
class GlobalConst
{

    // 消息推送平台密钥
    const SECRET_MSG_CENTER = 'i^!$K*ttZS@eDOT9dEmK^xs*z4pVozBQGos';
    // 签名过期时间
    const SIGN_EXPIRED_TIME = 30;

    // 消息推送页面密钥
    const SECRET_MSG_CENTER_PAGES = '7#KWYHy02JRUGHVpCi9r-Lu';
    // 页面签名过期时间
    const SIGN_EXPIRED_TIME_PAGES = 15;

    // 每页返回数量
    const BASE_PAGE_COUNT = 15;
    // 默认偏移量
    const DEFAULT_OFFSET = 0;
    // 默认分页数
    const DEFAULT_PAGE = 1;
    // 默认排序
    const DEFAULT_ORDER_TYPE = 'desc';
    // 默认排序字段
    const DEFAULT_ORDER_FIELD = 'id';


    // 默认查询30天以内的数据
    const DEFAULT_QUERY_TIME = 86400 * 30;

    // 错误文件标志位
    const ERROR_FILE_FLAG = '@file';

    // RabbitMQ
    const RMQ_EXCHANGE_MSG_CENTER = 'message_center';
    const RMQ_QUEUE_MSG_CENTER = 'message_push_list';
    const RMQ_QUEUE_TYPE_DINGTALK = 'dingtalk';
    const RMQ_QUEUE_TYPE_MSG = 'sms';


}
