<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\RabbitMQWorkCommand::class,
        \App\Console\Commands\CreateMsgSendTableCommand::class,
        \App\Console\Commands\UpdateMessageConfigCommand::class,
        \App\Console\Commands\ScheduleWorkCommand::class,
        \App\Console\Commands\ZTestCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // update config every ten minute
        //$schedule->command('msg-config:update')->everyTenMinutes();
        // check mq status every five minute
        $schedule->command('rabbitmq:work', ['--action' => 'check'])->everyFiveMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
