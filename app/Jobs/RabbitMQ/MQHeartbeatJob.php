<?php

namespace App\Jobs\RabbitMQ;

use App\Jobs\BaseJob;
use Illuminate\Support\Facades\Artisan;

class MQHeartbeatJob extends BaseJob
{

    /**
     * Start mq
     *
     * @return int
     */
    public function startMq()
    {
        $res = Artisan::call('rabbitmq:work', ['--action' => 'start']);
        return $res;
    }

    /**
     * Stop mq
     *
     * @return array|int
     */
    public function stopMq()
    {
        $res = Artisan::call('rabbitmq:work', ['--action' => 'stop']);
        return $res;
    }

    /**
     * Reload mq
     *
     * @return int
     */
    public function reloadMq()
    {
        $res = Artisan::call('rabbitmq:work', ['--action' => 'reload']);
        return $res;
    }

}
