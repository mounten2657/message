<?php
/*
|--------------------------------------------------------------------------
| DingTalk Open Api
|--------------------------------------------------------------------------
|
|  Before using the capabilities of the DingTalk open platform to develop applications, please note:
|  When calling the DingTalk server interface, the HTTPS protocol, JSON data format, and UTF-8 encoding are required. The access domain name is https://oapi.dingtalk.com. For POST request, please set Content-Type:application/json in HTTP Header.
|  Before calling the server interface, make sure that you have understood the calling frequency limit. For details, please refer to the call frequency limit.
|  Before calling the server interface, make sure you have set the corresponding interface permissions. For details, please refer to Adding Interface Calling Permission.
|  Regardless of the application (H5 application or applet), it must be connected to Dingding. This means that when the user opens the application, the user can directly obtain the user's identity without entering the Dingding account and password. For details, please refer to:
|  No-logging authorization code.
|  If you are developing a small program application, make sure you have configured the small program HTTP domain name and developer on the open platform. For details, please refer to Configuration Application.
|  If you are developing H5 micro-applications, JSAPI authentication is required. For details, please refer to H5 micro-application JSAPI authentication.
|  It is recommended to configure event subscriptions. DingTalk will push subscribed events to the application, such as department changes, sign-in notifications, clock-in notifications, etc. By subscribing to these events, developers can better integrate with DingTalk. For details, please refer to Configuring Event Subscription.
|
*/

return [

    // 钉钉工作通知消息
    'notice' => [
        'agent_id'    => env('DING_NOTICE_AGENT_ID',''),
        'app_key'     => env('DING_NOTICE_APP_KEY',''),
        'app_secret'  => env('DING_NOTICE_APP_SECRET',''),
    ],

    // 默认发送的机器人
    'default' => [
        // 是否要开启机器人，关闭则不再发送消息
        'enabled' => env('DING_ENABLED',true),
        // 机器人的access_token
        'token' => env('DING_TOKEN',''),
        // 钉钉请求的超时时间
        'timeout' => env('DING_TIME_OUT',2.0),
        // 是否开启ss认证
        'ssl_verify' => env('DING_SSL_VERIFY',true),
        // 开启安全配置
        'secret' => env('DING_SECRET',true),
        // 关键字前缀
        'prefix' => env('DING_PREFIX',''),
    ],

    // 其他机器人
    'other' => [
        'enabled' => env('OTHER_DING_ENABLED',true),

        'token' => env('OTHER_DING_TOKEN',''),

        'timeout' => env('OTHER_DING_TIME_OUT',2.0),

        'ssl_verify' => env('DING_SSL_VERIFY',true),

        'secret' => env('DING_SECRET',true),
    ]

];
