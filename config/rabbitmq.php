<?php
/*
|--------------------------------------------------------------------------
| RabbitMQ Config
|--------------------------------------------------------------------------
|
|  With tens of thousands of users, RabbitMQ is one of the most popular open source message brokers.
|  From T-Mobile to Runtastic, RabbitMQ is used worldwide at small startups and large enterprises.
|  RabbitMQ is lightweight and easy to deploy on premises and in the cloud. It supports multiple
|  messaging protocols. RabbitMQ can be deployed in distributed and federated configurations to
|  meet high-scale, high-availability requirements.
|  RabbitMQ runs on many operating systems and cloud environments, and provides a wide range of
|  developer tools for most popular languages.
|
*/

return [

    // 默认配置
    'default' => [
        'host' => env('RABBITMQ_HOST', 'localhost'),
        'port' => env('RABBITMQ_PORT', '5672'),
        'login' => env('RABBITMQ_USER', ''),
        'password' => env('RABBITMQ_PASS', ''),
        'vhost' => env('RABBITMQ_VHOST', '/'),
        'http_url' => env('RABBITMQ_HTTP_URL', 'https://demo.chnnice.com/rabbitmq/#/'),
    ],

];
